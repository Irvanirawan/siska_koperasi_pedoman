@extends('report.layout.master')

@section('judul')
<section class="content-header">
      <h1>
        Beranda
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class="active"></li>
      </ol>
    </section>
@endsection

@section('konten')
<!-- TO DO List -->
          <div class="box box-primary">
            <div class="box-header">
              <!-- <div class="box-tools pull-right">
                <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i></button>
              </div> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
              <div class="col-sm-12">
<h3 class="center-block">Selamat Datang Di Web Koperasi Pedoman</h3>
<p>Aplikasi ini dibuat untuk membantu mengetahui informasi tentang kegiatan koperasi Pedoman per anggota.</p> <br>
<p></p>
              </div>
            </div>
            <!-- /.box-body -->
            <!-- <div class="box-footer clearfix no-border">
              <button type="button" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>
            </div> -->
          </div>
          <!-- /.box -->
@endsection

@section('script')

<script src="{{asset('js/loadingoverlay.min.js')}}"></script>
<script type="text/javascript">
  
</script>
@endsection