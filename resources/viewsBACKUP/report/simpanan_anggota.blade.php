@extends('report.layout.master')

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
@endsection

@section('judul')
<section class="content-header">
      <h1>
        Laporan 
        <small>Simpanan Anggota</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Laporan Simpanan Anggota</li>
      </ol>
    </section>
@endsection

@section('konten')
<div class="row">
	<div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title pull-left hku" style="margin-bottom: 20px;">Nama Anggota : {{DB::table('jm_pelanggan_h')->where('KODE_PELANGGAN','=',Session::get('user')['kds'])->first()->NAMA_PELANGGAN}}</h3>
            <div class="pull-right">
            <form class="form-inline" method="post" action="{{route('simpanan-anggota-post')}}">
              <div class="form-group">
                <select name="jenis" id="jenis" class="form-control input-sm select2" style="margin-right: 10px" required>
                  <option value="">-Pilih Jenis Simpanan-</option>
              @foreach(DB::table('kpr_m_jenissimpanan')->get() as $k=>$d)
                  <option value="{{$d->Kode_simpanan}}">{{$d->Nama_simpanan}}</option>
              @endforeach
                </select>
              </div>
              <button type="submit" class="btn btn-sm btn-primary" id="carijenis"><i class="fa fa-search"></i> Cari</button>
            </form>
              <!-- <div class="row" style="margin-right: 10px;">
                <div class="col-xs-5" style="padding-left: 5px;padding-right: 5px">
                  <input type="text" class="form-control input-sm" id="datepicker" placeholder="Dari">
                </div>
                <div class="col-xs-5" style="padding-left: 5px;padding-right: 5px">
                  <input type="text" class="form-control input-sm" id="datepicker2" placeholder="Sampai">
                </div>
                <div class="col-xs-2" style="padding-left: 5px;padding-right: 5px">
                  <button type="button" class="btn btn-sm btn-primary" id="datepicker2"><i class="fa fa-search"></i> Cari</button>
                </div>
              </div> -->
            </div>  
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
              <thead>
                <tr>
                  <th>Tanggal</th>
                  <th>No Simpanan</th>
                  <th>Jenis Simpanan</th>
                  <th>Jumlah</th>
                </tr>
              </thead>
              <tbody>
@forelse($data as $key=>$simpanan)
                <tr>
                  <td>{{$simpanan->TglBukti}}</td>
                  <td>{{$simpanan->NoBukti}}</td>
                  <td>{{$simpanan->JenisKartu}}</td>
                  <td>{{"Rp. ".number_format($simpanan->Jumlah, 2)}}</td>
                </tr>
@empty
<tr class="odd"><td valign="top" colspan="7" style="text-align: center;">Data Kosong / Pilih Jenis</td></tr>
@endforelse
</tbody>
@if(count($data) != 0)
<tfoot>
  <tr>
    <th colspan="2"></th>
    <th >TOTAL :</th>
    <th>{{"Rp. ".number_format($data->sum('Jumlah'), 2)}}</th>
  </tr>
</tfoot>
@else
@endif
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
@if(count($data) != 0)
              <button type="button" class="btn btn-sm btn-info" id="datepicker2"><i class="fa fa-file-pdf-o"></i> Download</button>
              <button type="button" class="btn btn-sm btn-info" id="datepicker2"><i class="fa fa-print"></i> Cetak</button>
@else
@endif
            </div>
          </div>
          <!-- /.box -->
        </div>
</div>
@endsection

@section('script')
<!-- DataTables -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('AdminLTE-2.4.5/dist/js/demo.js')}}"></script>
<!-- loading overlay Gaspare Sganga -->
<script src="{{asset('js/loadingoverlay.min.js')}}"></script>
<script>
$("#carijenis").click(function(){
  if ($('#jenis').val() != '') {
    $.LoadingOverlay("show");
  }
    
});
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
@endsection