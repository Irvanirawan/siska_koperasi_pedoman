<html> 
<head> 
    <title>Laporan Belanja</title> 
    <style type="text/css"> 
        body { 
            font-family: 'Arial'; 
            font-size: 16px; 
        } 
        
        @media screen 
        { 
            p 
            { 
                font-family: Verdana; 
                font-size: 14px; 
                font-style: italic; 
                color: Green; 
            } 
        } 
        @media print 
        { 
            p 
            { 
                font-family: 'Courier New';
                font-size: 12px; 
                color: Black; 
            } 
        } 
        @media screen,print 
        { 
            p 
            { 
                font-weight: bold; 
            } 
        } 
    </style> 
</head> 
<body> 
<table>
              <thead>
                <tr>
                  <th>Tanggal</th>
                  <th>No Faktur</th>
                  <th>Nama Barang</th>
                  <th>Satuan</th>
                  <th>Jumlah</th>
                  <th>Harga Satuan</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
@forelse($data as $key=>$belanjaan)
                <tr>
                  <td>{{$belanjaan->TGL_FAKTUR}}</td>
                  <td>{{$belanjaan->NO_FAKTUR}}</td>
                  <td>{{$belanjaan->NAMA_BARANG}}</td>
                  <td>{{$belanjaan->SATUAN}}</td>
                  <td>{{$belanjaan->QTY_FAKTUR}}</td>
                  <td>{{$belanjaan->HARSAT_FAKTUR}}</td>
                  <td>{{$belanjaan->QTY_FAKTUR*$belanjaan->HARSAT_FAKTUR}}</td>
                </tr>
@empty
<tr class="odd"><td valign="top" colspan="7" style="text-align: center;">Data Kosong / Pilih Tanggal</td></tr>
@endforelse
</tbody>
@if(count($data) != 0)
<tfoot>
  <tr>
    <th colspan="5"></th>
    <th >TOTAL :</th>
    <th>{{$data->sum('total')}}</th>
  </tr>
</tfoot>
@else
@endif
              </table>
</body> 
</html>