@extends('report.layout.master')

@section('css')
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
@endsection

@section('judul')
<section class="content-header">
      <h1>
        Laporan 
        <small>Belanja Anggota</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Laporan Belanja Anggota</li>
      </ol>
    </section>
@endsection

@section('konten')
<div class="row">
	<div class="col-md-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title pull-left hku" style="margin-bottom: 20px;">Nama Anggota : {{DB::table('as_staff')->where('KODE_STAFF','=',Session::get('user')['kds'])->first()->NAMA_STAFF}}</h3>
            <div class="pull-right">
              <div class="row" style="margin-right: 10px;">
              <form method="post" action="{{route('belanja-anggota-post')}}" autocomplete="off">
                <div class="col-xs-5" style="padding-left: 5px;padding-right: 5px">
                  <input type="text" name="dari" class="form-control input-sm" id="datepicker" value="{{$dari}}" placeholder="Dari" required>
                </div>
                <div class="col-xs-5" style="padding-left: 5px;padding-right: 5px">
                  <input type="text" name="sampai" class="form-control input-sm" id="datepicker2" value="{{$sampai}}" placeholder="Sampai" required>
                </div>
                <div class="col-xs-2" style="padding-left: 5px;padding-right: 5px">
                  <button type="submit" class="btn btn-sm btn-primary" id="datepicker3"><i class="fa fa-search"></i> Cari</button>
                </div>
                </form>
              </div>
            </div>  
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
              <thead>
                <tr>
                  <th>Tanggal</th>
                  <th>No Faktur</th>
                  <th>Nama Barang</th>
                  <th>Satuan</th>
                  <th>Jumlah</th>
                  <th>Harga Satuan</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
@forelse($data as $key=>$belanjaan)
                <tr>
                  <td>{{$belanjaan->TGL_FAKTUR}}</td>
                  <td>{{$belanjaan->NO_FAKTUR}}</td>
                  <td>{{$belanjaan->NAMA_BARANG}}</td>
                  <td>{{$belanjaan->SATUAN}}</td>
                  <td>{{$belanjaan->QTY_FAKTUR}}</td>
                  <td>{{$belanjaan->HARSAT_FAKTUR}}</td>
                  <td>{{$belanjaan->QTY_FAKTUR*$belanjaan->HARSAT_FAKTUR}}</td>
                </tr>
@empty
<tr class="odd"><td valign="top" colspan="7" style="text-align: center;">Data Kosong / Pilih Tanggal</td></tr>
@endforelse
</tbody>
@if(count($data) != 0)
<tfoot>
  <tr>
    <th colspan="5"></th>
    <th >TOTAL :</th>
    <th>{{$data->sum('total')}}</th>
  </tr>
</tfoot>
@else
@endif
              </table>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
@if(count($data) != 0)
              <button type="button" class="btn btn-sm btn-info" id="datepicker2"><i class="fa fa-file-pdf-o"></i> Download</button>
              <button type="button" class="btn btn-sm btn-info" id="datepicker2"><i class="fa fa-print"></i> Cetak</button>
@else

@endif
            </div>
          </div>
          <!-- /.box -->
        </div>
</div>
@endsection

@section('script')
<!-- bootstrap datepicker -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- loading overlay Gaspare Sganga -->
<script src="{{asset('js/loadingoverlay.min.js')}}"></script>
<script type="text/javascript">
$("#datepicker3").click(function(){
  if ($("#datepicker").val().length != 0 && $("#datepicker2").val().length != 0) {
    $.LoadingOverlay("show");
  }
    
});

$(function () {
      //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      format:'yyyy-mm-dd',
      autoComplete:false
    })
    $('#datepicker2').datepicker({
      autoclose: true,
      format:'yyyy-mm-dd',
      autoComplete:false
    })
});
</script>
@endsection