@extends('admin.layout.master')

@section('title')
@parent
 | ukuran
@stop

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <meta name="_token" content="{{csrf_token()}}" />
@endsection

@section('judul')
<section class="content-header">
      <h1>
        Master
        <small>ukuran</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Data</a></li>
      </ol>
    </section>
@endsection

@section('konten')
<div class="box">
            <div class="box-header">
            	<div class="pull-left">
              		<h3 class="box-title"><i class="fa fa-bookmark-o"></i> ukuran</h3>
              	</div>
            	<div class="pull-right">
            		<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#formtambahukuran"><i class="fa fa-pencil-square-o"></i> Tambah</a>
              	</div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabelukuran" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Aksi</th>
                  <th>Kode ukuran</th>
                  <th>Nama ukuran</th>
                  <th>Dibuat Oleh</th>
                  <th>Tanggal Dibuat</th>
                  <th>Tanggal Diubah</th>
                  <th>Tanggal Dihapus</th>
                </tr>
                </thead>
                <tbody>
                
            	<tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
<!-- ===========================      MODAL BOOTSTRAP    ======================================================================================== -->
<div id="formtambahukuran" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-check-square-o"></i> Data Ukuran</h4>
      </div>
      <div class="modal-body">
      	<div class="alert alert-success" id="alert" style="display:none"></div>
<form class="form-horizontal" id="formukuran">
	  <div class="form-group">
	    <label class="control-label col-sm-3">Kode ukuran</label>
	    <div class="col-sm-8">
	      <input type="text" class="form-control" id="kodeukuran" placeholder="Kode ukuran">
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="control-label col-sm-3">Nama ukuran</label>
	    <div class="col-sm-8">
	      <input type="text" class="form-control" id="namaukuran" placeholder="Nama ukuran">
	    </div>
	  </div>
      </div>
      <div class="modal-footer">
        <button id="simpanukuran" type="submit" name="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i>Simpan</button>
        <button id="resetukuran" type="button" class="btn btn-warning"><i class="fa fa-retweet"></i>Reset</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-window-close"></i>Tutup</button>
      </div>
    </div>
</form>
  </div>
</div>
<!-- ===========================      MODAL BOOTSTRAP    ======================================================================================== -->
<div id="formeditukuran" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-check-square-o"></i> Data Barang <small>edit</small></h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-success" id="alert" style="display:none"></div>
<form class="form-horizontal" id="formeditukuran">
    <div class="form-group">
      <label class="control-label col-sm-3">Kode ukuran</label>
      <div class="col-sm-8">
        <input type="hidden" class="form-control" id="editidukuran">
        <input type="text" class="form-control" id="editkodeukuran">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3">Nama ukuran</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="editnamaukuran">
      </div>
    </div>
      </div>
      <div class="modal-footer">
        <button id="editsimpanukuran" type="submit" name="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i>Simpan</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-window-close"></i>Tutup</button>
      </div>
    </div>
</form>
  </div>
</div>
@endsection

@section('script')
<!-- DataTables -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLTE-2.4.5/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('AdminLTE-2.4.5/dist/js/demo.js')}}"></script>
<!-- notify.min -->
<script src="{{asset('js/notify.min.js')}}"></script>
<script src="{{asset('js/notify.js')}}"></script>
<script src="{{asset('js/sweetalert.min.js')}}"></script>
<!-- page script -->
<script>
$('#tabelukuran').DataTable({
          "processing": true,
          "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
          responsive: true,
          "order": [[ 4, "desc" ]],
          language: {
            "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
            "sSearchPlaceholder": "Search any..",
            "search": "_INPUT_",
          },
          serverSide: true,
          ajax: '/master/ukuran/data',
          columns: [
            {"data": {id_ukuran:"id_ukuran",dihapus:"dihapus"},"searchable":"false",
               "render": function(data, type, row, meta){
                if (data.dihapus === null) {
                   var button = '<button data-toggle="modal" data-target="#formeditukuran" id="buttoneditukuran" data-value="'+data.id_ukuran+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeleteukuran" data-value="'+data.id_ukuran+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                }else{
               	var button = '';
                }
                return button;
               }
            },
            {data: 'kode_ukuran'},
            {data: 'nama_ukuran'},
            {data: 'name'},
            {data: 'dibuat'},
            {data: 'diubah'},
            {"data": "dihapus",
               "render": function(data, type, row, meta){
                if(data === null){
                  data = 'Belum Dihapus';
                }
                return data;
               }
            }
          ]
        });

$(document).ready(function(){
            $('#simpanukuran').click(function(e){
              $('#simpanukuran').prop('disabled', true);
              $("#simpanukuran").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');              
               e.preventDefault();
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
               if ( isNaN($('#kodeukuran').val()) && isNaN($('#namaukuran').val()) ) {
               $.ajax({
                  url: "{!! route('tambahukurandata') !!}",
                  method: 'post',
                  data: {
                     kodeukuran: $('#kodeukuran').val(),
                     namaukuran: $('#namaukuran').val()
                  },
                  success: function(result){
                    swal("Data Berhasil Disimpan!");
                    $('#simpanukuran').prop('disabled', false);
                    $("#simpanukuran").html('<i class="fa fa-check-square-o"></i>Simpan');
                    if ( $.fn.dataTable.isDataTable( '#tabelukuran' ) ) {
                          table = $('#tabelukuran').DataTable();
                          table.destroy();
                      }
                      $(function() {
                          $('#tabelukuran').DataTable({
                                "processing": true,
                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                responsive: true,
                                "order": [[ 4, "desc" ]],
                                language: {
                                  "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                  "sSearchPlaceholder": "Search any..",
                                  "search": "_INPUT_",
                                },
                                serverSide: true,
                                ajax: '/master/ukuran/data',
                                columns: [
                                  {"data": {id_ukuran:"id_ukuran",dihapus:"dihapus"},"searchable":"false",
                                     "render": function(data, type, row, meta){
                                      if (data.dihapus === null) {
                                          var button = '<button data-toggle="modal" data-target="#formeditukuran" id="buttoneditukuran" data-value="'+data.id_ukuran+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeleteukuran" data-value="'+data.id_ukuran+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                                      }else{
                                          var button = '';
                                      }
                                       return button;
                                     }
                                  },
                                  {data: 'kode_ukuran'},
                                  {data: 'nama_ukuran'},
                                  {data: 'name'},
                                  {data: 'dibuat'},
                                  {data: 'diubah'},
                                  {"data": "dihapus",
                                     "render": function(data, type, row, meta){
                                      if(data === null){
                                        data = 'Belum Dihapus';
                                      }
                                      return data;
                                     }
                                  }
                                ]
                              });
                      });
                  }
              	    });
                  }else{
                        swal({
                              title: "Kesalahan!",
                              text: "Form Tidak Boleh Kosong!",
                              icon: "warning",
                              button: "Input Ulang!",
                            });
                        $("#simpanukuran").html('<i class="fa fa-check-square-o"></i>Simpan');
                        $('#kodeukuran').focus(); $('#kodeukuran').focus();
                        $('#simpanukuran').prop('disabled', false);    
                       }
               });
            
$('#resetukuran').click(function(e){
    $('#formukuran').trigger("reset");
  });

            });

$(document).on('click', '#buttoneditukuran', function(){
  $("#editsimpanukuran").prop('disabled', true);
  $("#editkodeukuran").prop('disabled', true);
  $("#editnamaukuran").prop('disabled', true);
  $("#editsimpanukuran").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
      $.ajax({
              url: "/master/ukuran/data/"+$(this).data("value"),
              method: 'get',
              success: function(result){
                $("#editsimpanukuran").html('<i class="fa fa-check-square-o"></i>Simpan');
                $("#editsimpanukuran").prop('disabled', false);
                $("#editkodeukuran").prop('disabled', false);
                $("#editnamaukuran").prop('disabled', false);
                $('#editidukuran').val(result.id_ukuran);
                $('#editkodeukuran').val(result.kode_ukuran);
                $('#editnamaukuran').val(result.nama_ukuran);
              }
            });
});

$(document).on('click', '#editsimpanukuran', function(){
    $('#editsimpanukuran').prop('disabled', true);
    $("#editsimpanukuran").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
    $.ajax({
              url: "{!! route('editukurandata') !!}",
              method: 'post',
              data: {
                 kodeukuran: $('#editkodeukuran').val(),
                 namaukuran: $('#editnamaukuran').val(),
                 idukuran: $('#editidukuran').val()
              },
              success: function(result){
                $('#editsimpanukuran').prop('disabled', false);
                $("#editsimpanukuran").html('<i class="fa fa-check-square-o"></i>Simpan');
                swal('Data Tersimpan');
                if ( $.fn.dataTable.isDataTable( '#tabelukuran' ) ) {
                          table = $('#tabelukuran').DataTable();
                          table.destroy();
                      }
                      $(function() {
                          $('#tabelukuran').DataTable({
                                "processing": true,
                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                responsive: true,
                                "order": [[ 4, "desc" ]],
                                language: {
                                  "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                  "sSearchPlaceholder": "Search any..",
                                  "search": "_INPUT_",
                                },
                                serverSide: true,
                                ajax: '/master/ukuran/data',
                                columns: [
                                  {"data": {id_ukuran:"id_ukuran",dihapus:"dihapus"},"searchable":"false",
                                     "render": function(data, type, row, meta){
                                      if (data.dihapus === null) {
                                          var button = '<button data-toggle="modal" data-target="#formeditukuran" id="buttoneditukuran" data-value="'+data.id_ukuran+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeleteukuran" data-value="'+data.id_ukuran+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                                      }else{
                                          var button = '';
                                      }
                                      return button;
                                     }
                                  },
                                  {data: 'kode_ukuran'},
                                  {data: 'nama_ukuran'},
                                  {data: 'name'},
                                  {data: 'dibuat'},
                                  {data: 'diubah'},
                                  {"data": "dihapus",
                                     "render": function(data, type, row, meta){
                                      if(data === null){
                                        data = 'Belum Dihapus';
                                      }
                                      return data;
                                     }
                                  }
                                ]
                              });
                      });
              }
            });

});

$(document).on('click', '#buttondeleteukuran', function(){
  $(this).prop('disabled', true);
  $(this).html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
  var targt = $(this);
  $.ajax({
          url: "/master/ukuran/data/"+$(this).data("value"),
          method: 'get',
          success: function(result){
            targt.prop('disabled', false);
            targt.html('<i class="fa fa-trash"></i>');
            swal({
                  title: "Yakin Hapus?",
                  text: "Kode ukuran: "+result.kode_ukuran+" ("+result.nama_ukuran+").",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    $.ajax({
                            url: "/master/ukuran/data/hapus/"+result.id_ukuran,
                            method: 'get',
                            success: function(result){
                              swal("Data Telah Dihapus",{
                                      icon: "success",
                                    });
                              if ( $.fn.dataTable.isDataTable( '#tabelukuran' ) ) {
                          table = $('#tabelukuran').DataTable();
                          table.destroy();
                      }
                      $(function() {
                          $('#tabelukuran').DataTable({
                                "processing": true,
                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                responsive: true,
                                "order": [[ 4, "desc" ]],
                                language: {
                                  "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                  "sSearchPlaceholder": "Search any..",
                                  "search": "_INPUT_",
                                },
                                serverSide: true,
                                ajax: '/master/ukuran/data',
                                columns: [
                                  {"data": {id_ukuran:"id_ukuran",dihapus:"dihapus"},"searchable":"false",
                                     "render": function(data, type, row, meta){
                                        if (data.dihapus === null) {
                                          var button = '<button data-toggle="modal" data-target="#formeditukuran" id="buttoneditukuran" data-value="'+data.id_ukuran+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeleteukuran" data-value="'+data.id_ukuran+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                                      }else{
                                          var button = '';
                                      }
                                      return button;
                                     }
                                  },
                                  {data: 'kode_ukuran'},
                                  {data: 'nama_ukuran'},
                                  {data: 'name'},
                                  {data: 'dibuat'},
                                  {data: 'diubah'},
                                  {"data": "dihapus",
                                     "render": function(data, type, row, meta){
                                      if(data === null){
                                        data = 'Belum Dihapus';
                                      }
                                      return data;
                                     }
                                  }
                                ]
                              });
                      });
                            }
                          });
                    
                  } else {
            targt.prop('disabled', false);
            targt.html('<i class="fa fa-trash"></i>');
                    swal("Hapus Data Batal");
                  }
                });
          }
        });
  
  
});
</script>
@endsection