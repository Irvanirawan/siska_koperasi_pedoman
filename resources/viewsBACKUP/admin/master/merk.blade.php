@extends('admin.layout.master')

@section('title')
@parent
 | merk
@stop

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <meta name="_token" content="{{csrf_token()}}" />
@endsection

@section('judul')
<section class="content-header">
      <h1>
        Master
        <small>merk</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Data</a></li>
      </ol>
    </section>
@endsection

@section('konten')
<div class="box">
            <div class="box-header">
            	<div class="pull-left">
              		<h3 class="box-title"><i class="fa fa-bookmark-o"></i> merk</h3>
              	</div>
            	<div class="pull-right">
            		<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#formtambahmerk"><i class="fa fa-pencil-square-o"></i> Tambah</a>
              	</div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabelmerk" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Aksi</th>
                  <th>Kode merk</th>
                  <th>Nama merk</th>
                  <th>Dibuat Oleh</th>
                  <th>Tanggal Dibuat</th>
                  <th>Tanggal Diubah</th>
                  <th>Tanggal Dihapus</th>
                </tr>
                </thead>
                <tbody>
                
            	<tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
<!-- ===========================      MODAL BOOTSTRAP    ======================================================================================== -->
<div id="formtambahmerk" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-check-square-o"></i> Data Merk</h4>
      </div>
      <div class="modal-body">
      	<div class="alert alert-success" id="alert" style="display:none"></div>
<form class="form-horizontal" id="formmerk">
	  <div class="form-group">
	    <label class="control-label col-sm-3">Kode merk</label>
	    <div class="col-sm-8">
	      <input type="text" class="form-control" id="kodemerk" placeholder="Kode merk">
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="control-label col-sm-3">Nama merk</label>
	    <div class="col-sm-8">
	      <input type="text" class="form-control" id="namamerk" placeholder="Nama merk">
	    </div>
	  </div>
      </div>
      <div class="modal-footer">
        <button id="simpanmerk" type="submit" name="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i>Simpan</button>
        <button id="resetmerk" type="button" class="btn btn-warning"><i class="fa fa-retweet"></i>Reset</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-window-close"></i>Tutup</button>
      </div>
    </div>
</form>
  </div>
</div>
<!-- ===========================      MODAL BOOTSTRAP    ======================================================================================== -->
<div id="formeditmerk" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-check-square-o"></i> Data Barang <small>edit</small></h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-success" id="alert" style="display:none"></div>
<form class="form-horizontal" id="formeditmerk">
    <div class="form-group">
      <label class="control-label col-sm-3">Kode merk</label>
      <div class="col-sm-8">
        <input type="hidden" class="form-control" id="editidmerk">
        <input type="text" class="form-control" id="editkodemerk">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3">Nama merk</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="editnamamerk">
      </div>
    </div>
      </div>
      <div class="modal-footer">
        <button id="editsimpanmerk" type="submit" name="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i>Simpan</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-window-close"></i>Tutup</button>
      </div>
    </div>
</form>
  </div>
</div>
@endsection

@section('script')
<!-- DataTables -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLTE-2.4.5/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('AdminLTE-2.4.5/dist/js/demo.js')}}"></script>
<!-- notify.min -->
<script src="{{asset('js/notify.min.js')}}"></script>
<script src="{{asset('js/notify.js')}}"></script>
<script src="{{asset('js/sweetalert.min.js')}}"></script>
<!-- page script -->
<script>
$('#tabelmerk').DataTable({
          "processing": true,
          "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
          responsive: true,
          "order": [[ 4, "desc" ]],
          language: {
            "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
            "sSearchPlaceholder": "Search any..",
            "search": "_INPUT_",
          },
          serverSide: true,
          ajax: '/master/merk/data',
          columns: [
            {"data": {id_merk:"id_merk",dihapus:"dihapus"},"searchable":"false",
               "render": function(data, type, row, meta){
                if (data.dihapus === null) {
                   var button = '<button data-toggle="modal" data-target="#formeditmerk" id="buttoneditmerk" data-value="'+data.id_merk+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeletemerk" data-value="'+data.id_merk+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                }else{
               	var button = '';
                }
                return button;
               }
            },
            {data: 'kode_merk'},
            {data: 'nama_merk'},
            {data: 'name'},
            {data: 'dibuat'},
            {data: 'diubah'},
            {"data": "dihapus",
               "render": function(data, type, row, meta){
                if(data === null){
                  data = 'Belum Dihapus';
                }
                return data;
               }
            }
          ]
        });

$(document).ready(function(){
            $('#simpanmerk').click(function(e){
              $('#simpanmerk').prop('disabled', true);
              $("#simpanmerk").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');              
               e.preventDefault();
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
               if ( isNaN($('#kodemerk').val()) && isNaN($('#namamerk').val()) ) {
               $.ajax({
                  url: "{!! route('tambahmerkdata') !!}",
                  method: 'post',
                  data: {
                     kodemerk: $('#kodemerk').val(),
                     namamerk: $('#namamerk').val()
                  },
                  success: function(result){
                    swal("Data Berhasil Disimpan!");
                    $('#simpanmerk').prop('disabled', false);
                    $("#simpanmerk").html('<i class="fa fa-check-square-o"></i>Simpan');
                    if ( $.fn.dataTable.isDataTable( '#tabelmerk' ) ) {
                          table = $('#tabelmerk').DataTable();
                          table.destroy();
                      }
                      $(function() {
                          $('#tabelmerk').DataTable({
                                "processing": true,
                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                responsive: true,
                                "order": [[ 4, "desc" ]],
                                language: {
                                  "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                  "sSearchPlaceholder": "Search any..",
                                  "search": "_INPUT_",
                                },
                                serverSide: true,
                                ajax: '/master/merk/data',
                                columns: [
                                  {"data": {id_merk:"id_merk",dihapus:"dihapus"},"searchable":"false",
                                     "render": function(data, type, row, meta){
                                      if (data.dihapus === null) {
                                          var button = '<button data-toggle="modal" data-target="#formeditmerk" id="buttoneditmerk" data-value="'+data.id_merk+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeletemerk" data-value="'+data.id_merk+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                                      }else{
                                          var button = '';
                                      }
                                       return button;
                                     }
                                  },
                                  {data: 'kode_merk'},
                                  {data: 'nama_merk'},
                                  {data: 'name'},
                                  {data: 'dibuat'},
                                  {data: 'diubah'},
                                  {"data": "dihapus",
                                     "render": function(data, type, row, meta){
                                      if(data === null){
                                        data = 'Belum Dihapus';
                                      }
                                      return data;
                                     }
                                  }
                                ]
                              });
                      });
                  }
              	    });
                  }else{
                        swal({
                              title: "Kesalahan!",
                              text: "Form Tidak Boleh Kosong!",
                              icon: "warning",
                              button: "Input Ulang!",
                            });
                        $("#simpanmerk").html('<i class="fa fa-check-square-o"></i>Simpan');
                        $('#kodemerk').focus(); $('#kodemerk').focus();
                        $('#simpanmerk').prop('disabled', false);    
                       }
               });
            
$('#resetmerk').click(function(e){
    $('#formmerk').trigger("reset");
  });

            });

$(document).on('click', '#buttoneditmerk', function(){
  $("#editsimpanmerk").prop('disabled', true);
  $("#editkodemerk").prop('disabled', true);
  $("#editnamamerk").prop('disabled', true);
  $("#editsimpanmerk").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
      $.ajax({
              url: "/master/merk/data/"+$(this).data("value"),
              method: 'get',
              success: function(result){
                $("#editsimpanmerk").html('<i class="fa fa-check-square-o"></i>Simpan');
                $("#editsimpanmerk").prop('disabled', false);
                $("#editkodemerk").prop('disabled', false);
                $("#editnamamerk").prop('disabled', false);
                $('#editidmerk').val(result.id_merk);
                $('#editkodemerk').val(result.kode_merk);
                $('#editnamamerk').val(result.nama_merk);
              }
            });
});

$(document).on('click', '#editsimpanmerk', function(){
    $('#editsimpanmerk').prop('disabled', true);
    $("#editsimpanmerk").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
    $.ajax({
              url: "{!! route('editmerkdata') !!}",
              method: 'post',
              data: {
                 kodemerk: $('#editkodemerk').val(),
                 namamerk: $('#editnamamerk').val(),
                 idmerk: $('#editidmerk').val()
              },
              success: function(result){
                $('#editsimpanmerk').prop('disabled', false);
                $("#editsimpanmerk").html('<i class="fa fa-check-square-o"></i>Simpan');
                swal('Data Tersimpan');
                if ( $.fn.dataTable.isDataTable( '#tabelmerk' ) ) {
                          table = $('#tabelmerk').DataTable();
                          table.destroy();
                      }
                      $(function() {
                          $('#tabelmerk').DataTable({
                                "processing": true,
                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                responsive: true,
                                "order": [[ 4, "desc" ]],
                                language: {
                                  "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                  "sSearchPlaceholder": "Search any..",
                                  "search": "_INPUT_",
                                },
                                serverSide: true,
                                ajax: '/master/merk/data',
                                columns: [
                                  {"data": {id_merk:"id_merk",dihapus:"dihapus"},"searchable":"false",
                                     "render": function(data, type, row, meta){
                                      if (data.dihapus === null) {
                                          var button = '<button data-toggle="modal" data-target="#formeditmerk" id="buttoneditmerk" data-value="'+data.id_merk+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeletemerk" data-value="'+data.id_merk+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                                      }else{
                                          var button = '';
                                      }
                                      return button;
                                     }
                                  },
                                  {data: 'kode_merk'},
                                  {data: 'nama_merk'},
                                  {data: 'name'},
                                  {data: 'dibuat'},
                                  {data: 'diubah'},
                                  {"data": "dihapus",
                                     "render": function(data, type, row, meta){
                                      if(data === null){
                                        data = 'Belum Dihapus';
                                      }
                                      return data;
                                     }
                                  }
                                ]
                              });
                      });
              }
            });

});

$(document).on('click', '#buttondeletemerk', function(){
  $(this).prop('disabled', true);
  $(this).html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
  var targt = $(this);
  $.ajax({
          url: "/master/merk/data/"+$(this).data("value"),
          method: 'get',
          success: function(result){
            targt.prop('disabled', false);
            targt.html('<i class="fa fa-trash"></i>');
            swal({
                  title: "Yakin Hapus?",
                  text: "Kode merk: "+result.kode_merk+" ("+result.nama_merk+").",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    $.ajax({
                            url: "/master/merk/data/hapus/"+result.id_merk,
                            method: 'get',
                            success: function(result){
                              swal("Data Telah Dihapus",{
                                      icon: "success",
                                    });
                              if ( $.fn.dataTable.isDataTable( '#tabelmerk' ) ) {
                          table = $('#tabelmerk').DataTable();
                          table.destroy();
                      }
                      $(function() {
                          $('#tabelmerk').DataTable({
                                "processing": true,
                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                responsive: true,
                                "order": [[ 4, "desc" ]],
                                language: {
                                  "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                  "sSearchPlaceholder": "Search any..",
                                  "search": "_INPUT_",
                                },
                                serverSide: true,
                                ajax: '/master/merk/data',
                                columns: [
                                  {"data": {id_merk:"id_merk",dihapus:"dihapus"},"searchable":"false",
                                     "render": function(data, type, row, meta){
                                        if (data.dihapus === null) {
                                          var button = '<button data-toggle="modal" data-target="#formeditmerk" id="buttoneditmerk" data-value="'+data.id_merk+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeletemerk" data-value="'+data.id_merk+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                                      }else{
                                          var button = '';
                                      }
                                      return button;
                                     }
                                  },
                                  {data: 'kode_merk'},
                                  {data: 'nama_merk'},
                                  {data: 'name'},
                                  {data: 'dibuat'},
                                  {data: 'diubah'},
                                  {"data": "dihapus",
                                     "render": function(data, type, row, meta){
                                      if(data === null){
                                        data = 'Belum Dihapus';
                                      }
                                      return data;
                                     }
                                  }
                                ]
                              });
                      });
                            }
                          });
                    
                  } else {
            targt.prop('disabled', false);
            targt.html('<i class="fa fa-trash"></i>');
                    swal("Hapus Data Batal");
                  }
                });
          }
        });
  
  
});
</script>
@endsection