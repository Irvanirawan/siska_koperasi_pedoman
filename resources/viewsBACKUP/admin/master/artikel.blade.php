@extends('admin.layout.master')

@section('title')
@parent
 | Artikel
@stop

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <meta name="_token" content="{{csrf_token()}}" />
@endsection

@section('judul')
<section class="content-header">
      <h1>
        Master
        <small>Artikel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Data</a></li>
      </ol>
    </section>
@endsection

@section('konten')
<div class="box">
            <div class="box-header">
            	<div class="pull-left">
              		<h3 class="box-title"><i class="fa fa-bookmark-o"></i> Artikel</h3>
              	</div>
            	<div class="pull-right">
            		<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#formtambahartikel"><i class="fa fa-pencil-square-o"></i> Tambah</a>
              	</div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabelartikel" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Aksi</th>
                  <th>Kode Artikel</th>
                  <th>Nama Artikel</th>
                  <th>Dibuat Oleh</th>
                  <th>Tanggal Dibuat</th>
                  <th>Tanggal Diubah</th>
                  <th>Tanggal Dihapus</th>
                </tr>
                </thead>
                <tbody>                
            	</tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
<!-- ===========================      MODAL BOOTSTRAP    ======================================================================================== -->
<div id="formtambahartikel" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-check-square-o"></i> Data Artikel</h4>
      </div>
      <div class="modal-body">
      	<div class="alert alert-success" id="alert" style="display:none"></div>
<form class="form-horizontal" id="formartikel">
	  <div class="form-group">
	    <label class="control-label col-sm-3">Kode Artikel</label>
	    <div class="col-sm-8">
	      <input type="text" class="form-control" id="kodeartikel" placeholder="Kode Artikel">
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="control-label col-sm-3">Nama Artikel</label>
	    <div class="col-sm-8">
	      <input type="text" class="form-control" id="namaartikel" placeholder="Nama Artikel">
	    </div>
	  </div>
      </div>
      <div class="modal-footer">
        <button id="simpanartikel" type="submit" name="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i>Simpan</button>
        <button id="resetartikel" type="button" class="btn btn-warning"><i class="fa fa-retweet"></i>Reset</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-window-close"></i>Tutup</button>
      </div>
    </div>
</form>
  </div>
</div>
<!-- ===========================      MODAL BOOTSTRAP    ======================================================================================== -->
<div id="formeditartikel" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-check-square-o"></i> Data Barang <small>edit</small></h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-success" id="alert" style="display:none"></div>
<form class="form-horizontal" id="formeditartikel">
    <div class="form-group">
      <label class="control-label col-sm-3">Kode Artikel</label>
      <div class="col-sm-8">
        <input type="hidden" class="form-control" id="editidartikel">
        <input type="text" class="form-control" id="editkodeartikel">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3">Nama Artikel</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="editnamaartikel">
      </div>
    </div>
      </div>
      <div class="modal-footer">
        <button id="editsimpanartikel" type="submit" name="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i>Simpan</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-window-close"></i>Tutup</button>
      </div>
    </div>
</form>
  </div>
</div>
@endsection

@section('script')
<!-- DataTables -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLTE-2.4.5/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('AdminLTE-2.4.5/dist/js/demo.js')}}"></script>
<!-- notify.min -->
<script src="{{asset('js/notify.min.js')}}"></script>
<script src="{{asset('js/notify.js')}}"></script>
<script src="{{asset('js/sweetalert.min.js')}}"></script>
<!-- page script -->
<script>
$('#tabelartikel').DataTable({
          "processing": true,
          "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
          responsive: true,
          "order": [[ 4, "desc" ]],
          language: {
            "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
            "sSearchPlaceholder": "Search any..",
            "search": "_INPUT_",
          },
          serverSide: true,
          ajax: '/master/artikeldata',
          columns: [
            {"data": {id_artikel:"id_artikel",dihapus:"dihapus"},"searchable":"false","orderable":"false",
               "render": function(data, type, row, meta){
                if (data.dihapus === null) {
                   var button = '<button data-toggle="modal" data-target="#formeditartikel" id="buttoneditartikel" data-value="'+data.id_artikel+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeleteartikel" data-value="'+data.id_artikel+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                }else{
               	var button = '';
                }
                return button;
               }
            },
            {data: 'kode_artikel'},
            {data: 'nama_artikel'},
            {data: 'name'},
            {data: 'dibuat'},
            {data: 'diubah'},
            {"data": "dihapus",
               "render": function(data, type, row, meta){
                if(data === null){
                  data = 'Belum Dihapus';
                }
                return data;
               }
            }
          ]
        });

$(document).ready(function(){
            $('#simpanartikel').click(function(e){
              $('#simpanartikel').prop('disabled', true);
              $("#simpanartikel").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');              
               e.preventDefault();
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
               if ( isNaN($('#kodeartikel').val()) && isNaN($('#namaartikel').val()) ) {
               $.ajax({
                  url: "{!! route('tambahartikeldata') !!}",
                  method: 'post',
                  data: {
                     kodeartikel: $('#kodeartikel').val(),
                     namaartikel: $('#namaartikel').val()
                  },
                  success: function(result){
                    swal("Data Berhasil Disimpan!");
                    $('#simpanartikel').prop('disabled', false);
                    $("#simpanartikel").html('<i class="fa fa-check-square-o"></i>Simpan');
                    if ( $.fn.dataTable.isDataTable( '#tabelartikel' ) ) {
                          table = $('#tabelartikel').DataTable();
                          table.destroy();
                      }
                      $(function() {
                          $('#tabelartikel').DataTable({
                                "processing": true,
                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                responsive: true,
                                "order": [[ 4, "desc" ]],
                                language: {
                                  "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                  "sSearchPlaceholder": "Search any..",
                                  "search": "_INPUT_",
                                },
                                serverSide: true,
                                ajax: '/master/artikeldata',
                                columns: [
                                  {"data": {id_artikel:"id_artikel",dihapus:"dihapus"},"searchable":"false",
                                     "render": function(data, type, row, meta){
                                      if (data.dihapus === null) {
                                          var button = '<button data-toggle="modal" data-target="#formeditartikel" id="buttoneditartikel" data-value="'+data.id_artikel+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeleteartikel" data-value="'+data.id_artikel+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                                      }else{
                                          var button = '';
                                      }
                                       return button;
                                     }
                                  },
                                  {data: 'kode_artikel'},
                                  {data: 'nama_artikel'},
                                  {data: 'name'},
                                  {data: 'dibuat'},
                                  {data: 'diubah'},
                                  {"data": "dihapus",
                                     "render": function(data, type, row, meta){
                                      if(data === null){
                                        data = 'Belum Dihapus';
                                      }
                                      return data;
                                     }
                                  }
                                ]
                              });
                      });
                  }
              	    });
                  }else{
                        swal({
                              title: "Kesalahan!",
                              text: "Form Tidak Boleh Kosong!",
                              icon: "warning",
                              button: "Input Ulang!",
                            });
                        $("#simpanartikel").html('<i class="fa fa-check-square-o"></i>Simpan');
                        $('#kodeartikel').focus(); $('#kodeartikel').focus();
                        $('#simpanartikel').prop('disabled', false);    
                       }
               });
            
$('#resetartikel').click(function(e){
    $('#formartikel').trigger("reset");
  });

            });

$(document).on('click', '#buttoneditartikel', function(){
  $("#editsimpanartikel").prop('disabled', true);
  $("#editkodeartikel").prop('disabled', true);
  $("#editnamaartikel").prop('disabled', true);
  $("#editsimpanartikel").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
      $.ajax({
              url: "/master/artikeldata/"+$(this).data("value"),
              method: 'get',
              success: function(result){
                $("#editsimpanartikel").html('<i class="fa fa-check-square-o"></i>Simpan');
                $("#editsimpanartikel").prop('disabled', false);
                $("#editkodeartikel").prop('disabled', false);
                $("#editnamaartikel").prop('disabled', false);
                $('#editidartikel').val(result.id_artikel);
                $('#editkodeartikel').val(result.kode_artikel);
                $('#editnamaartikel').val(result.nama_artikel);
              }
            });
});

$(document).on('click', '#editsimpanartikel', function(){
    $('#editsimpanartikel').prop('disabled', true);
    $("#editsimpanartikel").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
    $.ajax({
              url: "{!! route('editartikeldata') !!}",
              method: 'post',
              data: {
                 kodeartikel: $('#editkodeartikel').val(),
                 namaartikel: $('#editnamaartikel').val(),
                 idartikel: $('#editidartikel').val()
              },
              success: function(result){
                $('#editsimpanartikel').prop('disabled', false);
                $("#editsimpanartikel").html('<i class="fa fa-check-square-o"></i>Simpan');
                swal('Data Tersimpan');
                if ( $.fn.dataTable.isDataTable( '#tabelartikel' ) ) {
                          table = $('#tabelartikel').DataTable();
                          table.destroy();
                      }
                      $(function() {
                          $('#tabelartikel').DataTable({
                                "processing": true,
                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                responsive: true,
                                "order": [[ 4, "desc" ]],
                                language: {
                                  "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                  "sSearchPlaceholder": "Search any..",
                                  "search": "_INPUT_",
                                },
                                serverSide: true,
                                ajax: '/master/artikeldata',
                                columns: [
                                  {"data": {id_artikel:"id_artikel",dihapus:"dihapus"},"searchable":"false",
                                     "render": function(data, type, row, meta){
                                      if (data.dihapus === null) {
                                          var button = '<button data-toggle="modal" data-target="#formeditartikel" id="buttoneditartikel" data-value="'+data.id_artikel+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeleteartikel" data-value="'+data.id_artikel+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                                      }else{
                                          var button = '';
                                      }
                                      return button;
                                     }
                                  },
                                  {data: 'kode_artikel'},
                                  {data: 'nama_artikel'},
                                  {data: 'name'},
                                  {data: 'dibuat'},
                                  {data: 'diubah'},
                                  {"data": "dihapus",
                                     "render": function(data, type, row, meta){
                                      if(data === null){
                                        data = 'Belum Dihapus';
                                      }
                                      return data;
                                     }
                                  }
                                ]
                              });
                      });
              }
            });

});

$(document).on('click', '#buttondeleteartikel', function(){
  $(this).prop('disabled', true);
  $(this).html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
  var targt = $(this);
  $.ajax({
          url: "/master/artikeldata/"+$(this).data("value"),
          method: 'get',
          success: function(result){
            swal({
                  title: "Yakin Hapus?",
                  text: "Kode Artikel: "+result.kode_artikel+" ("+result.nama_artikel+").",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    $.ajax({
                            url: "/master/artikeldata/hapus/"+result.id_artikel,
                            method: 'get',
                            success: function(result){
                              swal("Data Telah Dihapus",{
                                      icon: "success",
                                    });
                              if ( $.fn.dataTable.isDataTable( '#tabelartikel' ) ) {
                          table = $('#tabelartikel').DataTable();
                          table.destroy();
                      }
                      $(function() {
                          $('#tabelartikel').DataTable({
                                "processing": true,
                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                responsive: true,
                                "order": [[ 4, "desc" ]],
                                language: {
                                  "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                  "sSearchPlaceholder": "Search any..",
                                  "search": "_INPUT_",
                                },
                                serverSide: true,
                                ajax: '/master/artikeldata',
                                columns: [
                                  {"data": {id_artikel:"id_artikel",dihapus:"dihapus"},"searchable":"false",
                                     "render": function(data, type, row, meta){
                                        if (data.dihapus === null) {
                                          var button = '<button data-toggle="modal" data-target="#formeditartikel" id="buttoneditartikel" data-value="'+data.id_artikel+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeleteartikel" data-value="'+data.id_artikel+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                                      }else{
                                          var button = '';
                                      }
                                      return button;
                                     }
                                  },
                                  {data: 'kode_artikel'},
                                  {data: 'nama_artikel'},
                                  {data: 'name'},
                                  {data: 'dibuat'},
                                  {data: 'diubah'},
                                  {"data": "dihapus",
                                     "render": function(data, type, row, meta){
                                      if(data === null){
                                        data = 'Belum Dihapus';
                                      }
                                      return data;
                                     }
                                  }
                                ]
                              });
                      });
                            }
                          });
                    
                  } else {
            targt.prop('disabled', false);
            targt.html('<i class="fa fa-trash"></i>');
                    swal("Hapus Data Batal");
                  }
                });
          }
        });
  
  
});
</script>
@endsection