      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU</li>
        <li class="{{ Request::is('home') ? 'active' : '' }}"><a href="{{route('home')}}"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
        <li class="treeview {{ Request::is('master/*') ? 'active menu-open' : '' }}">
          <a href="#"><i class="fa fa-desktop"></i> <span>Master Data</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
            <li class=" {{ Request::is('master/artikel') ? 'active' : '' }}"><a href="{{route('artikel')}}"><i class="fa fa-bookmark-o"></i>Artikel</a></li>
            <li class=" {{ Request::is('master/gudang') ? 'active' : '' }}"><a href="{{route('gudang')}}"><i class="fa fa-truck"></i>Gudang</a></li>
            <li class=" {{ Request::is('master/kategoribarang') ? 'active' : '' }}"><a href="{{route('kategoribarang')}}"><i class="fa fa-cube"></i>Kategori Barang</a></li>
            <li class=" {{ Request::is('master/merek') ? 'active' : '' }}"><a href="{{route('merk')}}"><i class="fa fa-trademark"></i>Merek</a></li>
            <li class=" {{ Request::is('master/motif') ? 'active' : '' }}"><a href="{{route('motif')}}"><i class="fa fa-snowflake-o"></i>Motif</a></li>
            <li class=" {{ Request::is('master/ukuran') ? 'active' : '' }}"><a href="{{route('ukuran')}}"><i class="fa fa-universal-access"></i>Ukuran</a></li>
            <li class=" {{ Request::is('master/warna') ? 'active' : '' }}"><a href="{{route('warna')}}"><i class="fa fa-paint-brush"></i>Warna</a></li>
          </ul>
        </li>
      </ul>
      <!-- /.sidebar-menu -->