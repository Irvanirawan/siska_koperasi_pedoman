@extends('report.layout.master')

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}" />
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <style type="text/css">

  </style>
@endsection

@section('judul')
<section class="content-header">
      <h1>
        Status
        <small>Pengajuan Pinjaman Anggota</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Status Pengajuan Pinjaman Anggota</li>
      </ol>
    </section>
@endsection

@section('konten')
<div class="box">
            <div class="box-header">
              <h3 class="box-title">History Pengajuan Pinjaman</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="statuslist" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No Bukti</th>
                  <th>Tanggal Pengajuan</th>
                  <th>Jenis Pinjaman</th>
                  <th>Besar Pinjaman</th>
                  <th>Jangka Waktu</th>
                  <th>Status</th>
                </tr>
                </thead>
                <tbody>
@foreach(DB::table("kpr_t_pengajuanpinjaman")->get() as $k=>$d)
                  <tr>
                      <td>{{$d->NoBukti}}</td>
                      <td>{{date('d-m-Y',strtotime($d->TglBukti))}}</td>
                      <td>{{$d->kode_JenisPinjaman}}</td>
                      <td>Rp. {{number_format($d->BesarPinjaman)}}</td>
                      <td>{{$d->JangkaWaktu}}</td>
                      <td>
                        @if($d->FLAG == 'T') <span data-id="{{$d->NoBukti}}" class='aksi btn label label-info' data-toggle="modal" data-target="#myModal">Pengajuan</span>
                        @elseif($d->FLAG == 'N') <span data-id="{{$d->NoBukti}}" class='aksi btn label label-warning' data-toggle="modal" data-target="#myModal">Tolak</span>
                        @elseif($d->FLAG == 'Y') <span data-id="{{$d->NoBukti}}" class='aksi btn label label-success' data-toggle="modal" data-target="#myModal">Setujui</span>
                        @else
                        @endif
                      </td>
                  </tr>
@endforeach
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Detail Pengajuan</h4>
      </div>
      <div class="modal-body">
        <div class="">
          <form class="form-horizontal" action="/action_page.php">
            
            <div class="form-group">
              <label class="control-label col-sm-3">No Bukti:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="nobukti" readonly>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-3">No Anggota:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="noanggota" readonly>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-3">Nama Anggota:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="namaanggota" readonly>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-3">Instansi:</label>
              <div class="col-sm-9">
                <input type="text" class="form-control" id="instansi" readonly>
              </div>
            </div>

          </form>
        </div>
        <div class="row">
          <div class="col-sm-6">
        <form method="POST" action="{{route('permintaan-pinjaman-aksi')}}">
        <input type='hidden' name='aksi' class='aksi' value="1">
        <input type='hidden' name='id' class='nobukti'>
        <button type="submit" id="btn-submit-setujui" class="setujui btn btn-primary">Setujui</button>
        </form>
          </div>
          <div class="col-sm-6">
        <form class="pull-right" method="POST" action="{{route('permintaan-pinjaman-aksi')}}">
        <input type='hidden' name='aksi' class='aksi' value="2">
        <input type='hidden' name='id' class='nobukti'>
        <button type="submit" id="btn-submit-tolak" class="tolak btn btn-danger">Tolak</button>
        </form>
          </div>
        </div>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-danger">Tolak</button>
      </div> -->
    </div>

  </div>
</div>
@endsection

@section('script')
<!-- DataTables -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('AdminLTE-2.4.5/dist/js/demo.js')}}"></script>
<!-- loading overlay Gaspare Sganga -->
<script src="{{asset('js/loadingoverlay.min.js')}}"></script>
<!-- loading overlay Gaspare Sganga -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/webshim/1.16.0/dev/polyfiller.js"></script> -->
<!-- DataTables -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
$("#carijenis").click(function(){
  if ($('#jenis').val() != '') {
    $.LoadingOverlay("show");
  }    
});

$('#statuslist').DataTable()

$(document).ready(function(){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $(".aksi").on('click', function() {
              var val = $(this).data("id");
              $(".nobukti").val(val);
                $.ajax({
                    /* the route pointing to the post function */
                    url: '{!! route("getanggota") !!}',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: {_token: CSRF_TOKEN, id:val},
                    dataType: 'JSON',
                    /* remind that 'data' is the response of the AjaxController */
                    success: function (data) {
                      console.log(data);
                      $('#nobukti').val(val);
                      $('#noanggota').val(data.KODE_PELANGGAN);
                      $('#namaanggota').val(data.NAMA_PELANGGAN);
                      $('#instansi').val(data.KODE_INSTANSI);
                    }
                }); 
            });

$('#btn-submit-setujui').on('click',function(e){
    e.preventDefault();
    var form = $(this).parents('form');
    swal({
            title: "Setujui ?",
            text: 'Klik Ok untuk melanjutkan.', 
            icon: "info",
            buttons: true,
            dangerMode: false,
        })
        .then((willDelete) => {
          if (willDelete) {
            swal("Pengajuan Disetujui!", {
              icon: "success",
            });
            form.submit();
          } else {
            swal("Aksi Dibatalkan!");
          }
        });
});

                  $('#btn-submit-tolak').on('click',function(e){
                        e.preventDefault();
                        var form = $(this).parents('form');
                        swal({
                                title: "Tolak ?",
                                text: 'Klik Ok untuk melanjutkan.', 
                                icon: "warning",
                                buttons: true,
                                dangerMode: true,
                            })
                            .then((willDelete) => {
                              if (willDelete) {
                                swal("Pengajuan Ditolak!", {
                                  icon: "success",
                                });
                                form.submit();
                              } else {
                                swal("Aksi Dibatalkan!");
                              }
                            });
                    });


});
</script>
@endsection