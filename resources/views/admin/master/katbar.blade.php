@extends('admin.layout.master')

@section('title')
@parent
 | katbar
@stop

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <meta name="_token" content="{{csrf_token()}}" />
@endsection

@section('judul')
<section class="content-header">
      <h1>
        Master
        <small>Kategori Barang</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Data</a></li>
      </ol>
    </section>
@endsection

@section('konten')
<div class="box">
            <div class="box-header">
            	<div class="pull-left">
              		<h3 class="box-title"><i class="fa fa-bookmark-o"></i> Kategori Barang</h3>
              	</div>
            	<div class="pull-right">
            		<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#formtambahkatbar"><i class="fa fa-pencil-square-o"></i> Tambah</a>
              	</div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabelkatbar" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Aksi</th>
                  <th>Kode katbar</th>
                  <th>Nama katbar</th>
                  <th>Dibuat Oleh</th>
                  <th>Tanggal Dibuat</th>
                  <th>Tanggal Diubah</th>
                  <th>Tanggal Dihapus</th>
                </tr>
                </thead>
                <tbody>
                
            	<tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
<!-- ===========================      MODAL BOOTSTRAP    ======================================================================================== -->
<div id="formtambahkatbar" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-check-square-o"></i> Data Kategori Barang</h4>
      </div>
      <div class="modal-body">
      	<div class="alert alert-success" id="alert" style="display:none"></div>
<form class="form-horizontal" id="formkatbar">
	  <div class="form-group">
	    <label class="control-label col-sm-3">Kode katbar</label>
	    <div class="col-sm-8">
	      <input type="text" class="form-control" id="kodekatbar" placeholder="Kode katbar">
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="control-label col-sm-3">Nama katbar</label>
	    <div class="col-sm-8">
	      <input type="text" class="form-control" id="namakatbar" placeholder="Nama katbar">
	    </div>
	  </div>
      </div>
      <div class="modal-footer">
        <button id="simpankatbar" type="submit" name="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i>Simpan</button>
        <button id="resetkatbar" type="button" class="btn btn-warning"><i class="fa fa-retweet"></i>Reset</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-window-close"></i>Tutup</button>
      </div>
    </div>
</form>
  </div>
</div>
<!-- ===========================      MODAL BOOTSTRAP    ======================================================================================== -->
<div id="formeditkatbar" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-check-square-o"></i> Data Barang <small>edit</small></h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-success" id="alert" style="display:none"></div>
<form class="form-horizontal" id="formeditkatbar">
    <div class="form-group">
      <label class="control-label col-sm-3">Kode katbar</label>
      <div class="col-sm-8">
        <input type="hidden" class="form-control" id="editidkatbar">
        <input type="text" class="form-control" id="editkodekatbar">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3">Nama katbar</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="editnamakatbar">
      </div>
    </div>
      </div>
      <div class="modal-footer">
        <button id="editsimpankatbar" type="submit" name="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i>Simpan</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-window-close"></i>Tutup</button>
      </div>
    </div>
</form>
  </div>
</div>
@endsection

@section('script')
<!-- DataTables -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLTE-2.4.5/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('AdminLTE-2.4.5/dist/js/demo.js')}}"></script>
<!-- notify.min -->
<script src="{{asset('js/notify.min.js')}}"></script>
<script src="{{asset('js/notify.js')}}"></script>
<script src="{{asset('js/sweetalert.min.js')}}"></script>
<!-- page script -->
<script>
$('#tabelkatbar').DataTable({
          "processing": true,
          "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
          responsive: true,
          "order": [[ 4, "desc" ]],
          language: {
            "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
            "sSearchPlaceholder": "Search any..",
            "search": "_INPUT_",
          },
          serverSide: true,
          ajax: '/master/kategoribarang/data',
          columns: [
            {"data": {id_katbar:"id_katbar",dihapus:"dihapus"},"searchable":"false",
               "render": function(data, type, row, meta){
                if (data.dihapus === null) {
                   var button = '<button data-toggle="modal" data-target="#formeditkatbar" id="buttoneditkatbar" data-value="'+data.id_katbar+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeletekatbar" data-value="'+data.id_katbar+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                }else{
               	var button = '';
                }
                return button;
               }
            },
            {data: 'kode_katbar'},
            {data: 'nama_katbar'},
            {data: 'name'},
            {data: 'dibuat'},
            {data: 'diubah'},
            {"data": "dihapus",
               "render": function(data, type, row, meta){
                if(data === null){
                  data = 'Belum Dihapus';
                }
                return data;
               }
            }
          ]
        });

$(document).ready(function(){
            $('#simpankatbar').click(function(e){
              $('#simpankatbar').prop('disabled', true);
              $("#simpankatbar").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');              
               e.preventDefault();
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
               if ( isNaN($('#kodekatbar').val()) && isNaN($('#namakatbar').val()) ) {
               $.ajax({
                  url: "{!! route('tambahkategoribarangdata') !!}",
                  method: 'post',
                  data: {
                     kodekatbar: $('#kodekatbar').val(),
                     namakatbar: $('#namakatbar').val()
                  },
                  success: function(result){
                    swal("Data Berhasil Disimpan!");
                    $('#simpankatbar').prop('disabled', false);
                    $("#simpankatbar").html('<i class="fa fa-check-square-o"></i>Simpan');
                    if ( $.fn.dataTable.isDataTable( '#tabelkatbar' ) ) {
                          table = $('#tabelkatbar').DataTable();
                          table.destroy();
                      }
                      $(function() {
                          $('#tabelkatbar').DataTable({
                                "processing": true,
                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                responsive: true,
                                "order": [[ 4, "desc" ]],
                                language: {
                                  "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                  "sSearchPlaceholder": "Search any..",
                                  "search": "_INPUT_",
                                },
                                serverSide: true,
                                ajax: '/master/kategoribarang/data',
                                columns: [
                                  {"data": {id_katbar:"id_katbar",dihapus:"dihapus"},"searchable":"false",
                                     "render": function(data, type, row, meta){
                                      if (data.dihapus === null) {
                                          var button = '<button data-toggle="modal" data-target="#formeditkatbar" id="buttoneditkatbar" data-value="'+data.id_katbar+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeletekatbar" data-value="'+data.id_katbar+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                                      }else{
                                          var button = '';
                                      }
                                       return button;
                                     }
                                  },
                                  {data: 'kode_katbar'},
                                  {data: 'nama_katbar'},
                                  {data: 'name'},
                                  {data: 'dibuat'},
                                  {data: 'diubah'},
                                  {"data": "dihapus",
                                     "render": function(data, type, row, meta){
                                      if(data === null){
                                        data = 'Belum Dihapus';
                                      }
                                      return data;
                                     }
                                  }
                                ]
                              });
                      });
                  }
              	    });
                  }else{
                        swal({
                              title: "Kesalahan!",
                              text: "Form Tidak Boleh Kosong!",
                              icon: "warning",
                              button: "Input Ulang!",
                            });
                        $("#simpankatbar").html('<i class="fa fa-check-square-o"></i>Simpan');
                        $('#kodekatbar').focus(); $('#kodekatbar').focus();
                        $('#simpankatbar').prop('disabled', false);    
                       }
               });
            
$('#resetkatbar').click(function(e){
    $('#formkatbar').trigger("reset");
  });

            });

$(document).on('click', '#buttoneditkatbar', function(){
  $("#editsimpankatbar").prop('disabled', true);
  $("#editkodekatbar").prop('disabled', true);
  $("#editnamakatbar").prop('disabled', true);
  $("#editsimpankatbar").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
      $.ajax({
              url: "/master/kategoribarang/data/"+$(this).data("value"),
              method: 'get',
              success: function(result){
                $("#editsimpankatbar").html('<i class="fa fa-check-square-o"></i>Simpan');
                $("#editsimpankatbar").prop('disabled', false);
                $("#editkodekatbar").prop('disabled', false);
                $("#editnamakatbar").prop('disabled', false);
                $('#editidkatbar').val(result.id_katbar);
                $('#editkodekatbar').val(result.kode_katbar);
                $('#editnamakatbar').val(result.nama_katbar);
              }
            });
});

$(document).on('click', '#editsimpankatbar', function(){
    $('#editsimpankatbar').prop('disabled', true);
    $("#editsimpankatbar").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
    $.ajax({
              url: "{!! route('editkategoribarangdata') !!}",
              method: 'post',
              data: {
                 kodekatbar: $('#editkodekatbar').val(),
                 namakatbar: $('#editnamakatbar').val(),
                 idkatbar: $('#editidkatbar').val()
              },
              success: function(result){
                $('#editsimpankatbar').prop('disabled', false);
                $("#editsimpankatbar").html('<i class="fa fa-check-square-o"></i>Simpan');
                swal('Data Tersimpan');
                if ( $.fn.dataTable.isDataTable( '#tabelkatbar' ) ) {
                          table = $('#tabelkatbar').DataTable();
                          table.destroy();
                      }
                      $(function() {
                          $('#tabelkatbar').DataTable({
                                "processing": true,
                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                responsive: true,
                                "order": [[ 4, "desc" ]],
                                language: {
                                  "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                  "sSearchPlaceholder": "Search any..",
                                  "search": "_INPUT_",
                                },
                                serverSide: true,
                                ajax: '/master/kategoribarang/data',
                                columns: [
                                  {"data": {id_katbar:"id_katbar",dihapus:"dihapus"},"searchable":"false",
                                     "render": function(data, type, row, meta){
                                      if (data.dihapus === null) {
                                          var button = '<button data-toggle="modal" data-target="#formeditkatbar" id="buttoneditkatbar" data-value="'+data.id_katbar+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeletekatbar" data-value="'+data.id_katbar+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                                      }else{
                                          var button = '';
                                      }
                                      return button;
                                     }
                                  },
                                  {data: 'kode_katbar'},
                                  {data: 'nama_katbar'},
                                  {data: 'name'},
                                  {data: 'dibuat'},
                                  {data: 'diubah'},
                                  {"data": "dihapus",
                                     "render": function(data, type, row, meta){
                                      if(data === null){
                                        data = 'Belum Dihapus';
                                      }
                                      return data;
                                     }
                                  }
                                ]
                              });
                      });
              }
            });

});

$(document).on('click', '#buttondeletekatbar', function(){
  $(this).prop('disabled', true);
  $(this).html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
  var targt = $(this);
  $.ajax({
          url: "/master/kategoribarang/data/"+$(this).data("value"),
          method: 'get',
          success: function(result){
            targt.prop('disabled', false);
            targt.html('<i class="fa fa-trash"></i>');
            swal({
                  title: "Yakin Hapus?",
                  text: "Kode katbar: "+result.kode_katbar+" ("+result.nama_katbar+").",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    $.ajax({
                            url: "/master/kategoribarang/data/hapus/"+result.id_katbar,
                            method: 'get',
                            success: function(result){
                              swal("Data Telah Dihapus",{
                                      icon: "success",
                                    });
                              if ( $.fn.dataTable.isDataTable( '#tabelkatbar' ) ) {
                          table = $('#tabelkatbar').DataTable();
                          table.destroy();
                      }
                      $(function() {
                          $('#tabelkatbar').DataTable({
                                "processing": true,
                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                responsive: true,
                                "order": [[ 4, "desc" ]],
                                language: {
                                  "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                  "sSearchPlaceholder": "Search any..",
                                  "search": "_INPUT_",
                                },
                                serverSide: true,
                                ajax: '/master/kategoribarang/data',
                                columns: [
                                  {"data": {id_katbar:"id_katbar",dihapus:"dihapus"},"searchable":"false",
                                     "render": function(data, type, row, meta){
                                        if (data.dihapus === null) {
                                          var button = '<button data-toggle="modal" data-target="#formeditkatbar" id="buttoneditkatbar" data-value="'+data.id_katbar+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeletekatbar" data-value="'+data.id_katbar+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                                      }else{
                                          var button = '';
                                      }
                                      return button;
                                     }
                                  },
                                  {data: 'kode_katbar'},
                                  {data: 'nama_katbar'},
                                  {data: 'name'},
                                  {data: 'dibuat'},
                                  {data: 'diubah'},
                                  {"data": "dihapus",
                                     "render": function(data, type, row, meta){
                                      if(data === null){
                                        data = 'Belum Dihapus';
                                      }
                                      return data;
                                     }
                                  }
                                ]
                              });
                      });
                            }
                          });
                    
                  } else {
            targt.prop('disabled', false);
            targt.html('<i class="fa fa-trash"></i>');
                    swal("Hapus Data Batal");
                  }
                });
          }
        });
  
  
});
</script>
@endsection