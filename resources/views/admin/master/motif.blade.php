@extends('admin.layout.master')

@section('title')
@parent
 | motif
@stop

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <meta name="_token" content="{{csrf_token()}}" />
@endsection

@section('judul')
<section class="content-header">
      <h1>
        Master
        <small>motif</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Data</a></li>
      </ol>
    </section>
@endsection

@section('konten')
<div class="box">
            <div class="box-header">
            	<div class="pull-left">
              		<h3 class="box-title"><i class="fa fa-bookmark-o"></i> motif</h3>
              	</div>
            	<div class="pull-right">
            		<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#formtambahmotif"><i class="fa fa-pencil-square-o"></i> Tambah</a>
              	</div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabelmotif" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Aksi</th>
                  <th>Kode motif</th>
                  <th>Nama motif</th>
                  <th>Dibuat Oleh</th>
                  <th>Tanggal Dibuat</th>
                  <th>Tanggal Diubah</th>
                  <th>Tanggal Dihapus</th>
                </tr>
                </thead>
                <tbody>
                
            	<tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
<!-- ===========================      MODAL BOOTSTRAP    ======================================================================================== -->
<div id="formtambahmotif" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-check-square-o"></i> Data Motif</h4>
      </div>
      <div class="modal-body">
      	<div class="alert alert-success" id="alert" style="display:none"></div>
<form class="form-horizontal" id="formmotif">
	  <div class="form-group">
	    <label class="control-label col-sm-3">Kode motif</label>
	    <div class="col-sm-8">
	      <input type="text" class="form-control" id="kodemotif" placeholder="Kode motif">
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="control-label col-sm-3">Nama motif</label>
	    <div class="col-sm-8">
	      <input type="text" class="form-control" id="namamotif" placeholder="Nama motif">
	    </div>
	  </div>
      </div>
      <div class="modal-footer">
        <button id="simpanmotif" type="submit" name="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i>Simpan</button>
        <button id="resetmotif" type="button" class="btn btn-warning"><i class="fa fa-retweet"></i>Reset</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-window-close"></i>Tutup</button>
      </div>
    </div>
</form>
  </div>
</div>
<!-- ===========================      MODAL BOOTSTRAP    ======================================================================================== -->
<div id="formeditmotif" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-check-square-o"></i> Data Barang <small>edit</small></h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-success" id="alert" style="display:none"></div>
<form class="form-horizontal" id="formeditmotif">
    <div class="form-group">
      <label class="control-label col-sm-3">Kode motif</label>
      <div class="col-sm-8">
        <input type="hidden" class="form-control" id="editidmotif">
        <input type="text" class="form-control" id="editkodemotif">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3">Nama motif</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="editnamamotif">
      </div>
    </div>
      </div>
      <div class="modal-footer">
        <button id="editsimpanmotif" type="submit" name="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i>Simpan</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-window-close"></i>Tutup</button>
      </div>
    </div>
</form>
  </div>
</div>
@endsection

@section('script')
<!-- DataTables -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLTE-2.4.5/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('AdminLTE-2.4.5/dist/js/demo.js')}}"></script>
<!-- notify.min -->
<script src="{{asset('js/notify.min.js')}}"></script>
<script src="{{asset('js/notify.js')}}"></script>
<script src="{{asset('js/sweetalert.min.js')}}"></script>
<!-- page script -->
<script>
$('#tabelmotif').DataTable({
          "processing": true,
          "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
          responsive: true,
          "order": [[ 4, "desc" ]],
          language: {
            "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
            "sSearchPlaceholder": "Search any..",
            "search": "_INPUT_",
          },
          serverSide: true,
          ajax: '/master/motif/data',
          columns: [
            {"data": {id_motif:"id_motif",dihapus:"dihapus"},"searchable":"false",
               "render": function(data, type, row, meta){
                if (data.dihapus === null) {
                   var button = '<button data-toggle="modal" data-target="#formeditmotif" id="buttoneditmotif" data-value="'+data.id_motif+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeletemotif" data-value="'+data.id_motif+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                }else{
               	var button = '';
                }
                return button;
               }
            },
            {data: 'kode_motif'},
            {data: 'nama_motif'},
            {data: 'name'},
            {data: 'dibuat'},
            {data: 'diubah'},
            {"data": "dihapus",
               "render": function(data, type, row, meta){
                if(data === null){
                  data = 'Belum Dihapus';
                }
                return data;
               }
            }
          ]
        });

$(document).ready(function(){
            $('#simpanmotif').click(function(e){
              $('#simpanmotif').prop('disabled', true);
              $("#simpanmotif").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');              
               e.preventDefault();
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
               if ( isNaN($('#kodemotif').val()) && isNaN($('#namamotif').val()) ) {
               $.ajax({
                  url: "{!! route('tambahmotifdata') !!}",
                  method: 'post',
                  data: {
                     kodemotif: $('#kodemotif').val(),
                     namamotif: $('#namamotif').val()
                  },
                  success: function(result){
                    swal("Data Berhasil Disimpan!");
                    $('#simpanmotif').prop('disabled', false);
                    $("#simpanmotif").html('<i class="fa fa-check-square-o"></i>Simpan');
                    if ( $.fn.dataTable.isDataTable( '#tabelmotif' ) ) {
                          table = $('#tabelmotif').DataTable();
                          table.destroy();
                      }
                      $(function() {
                          $('#tabelmotif').DataTable({
                                "processing": true,
                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                responsive: true,
                                "order": [[ 4, "desc" ]],
                                language: {
                                  "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                  "sSearchPlaceholder": "Search any..",
                                  "search": "_INPUT_",
                                },
                                serverSide: true,
                                ajax: '/master/motif/data',
                                columns: [
                                  {"data": {id_motif:"id_motif",dihapus:"dihapus"},"searchable":"false",
                                     "render": function(data, type, row, meta){
                                      if (data.dihapus === null) {
                                          var button = '<button data-toggle="modal" data-target="#formeditmotif" id="buttoneditmotif" data-value="'+data.id_motif+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeletemotif" data-value="'+data.id_motif+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                                      }else{
                                          var button = '';
                                      }
                                       return button;
                                     }
                                  },
                                  {data: 'kode_motif'},
                                  {data: 'nama_motif'},
                                  {data: 'name'},
                                  {data: 'dibuat'},
                                  {data: 'diubah'},
                                  {"data": "dihapus",
                                     "render": function(data, type, row, meta){
                                      if(data === null){
                                        data = 'Belum Dihapus';
                                      }
                                      return data;
                                     }
                                  }
                                ]
                              });
                      });
                  }
              	    });
                  }else{
                        swal({
                              title: "Kesalahan!",
                              text: "Form Tidak Boleh Kosong!",
                              icon: "warning",
                              button: "Input Ulang!",
                            });
                        $("#simpanmotif").html('<i class="fa fa-check-square-o"></i>Simpan');
                        $('#kodemotif').focus(); $('#kodemotif').focus();
                        $('#simpanmotif').prop('disabled', false);    
                       }
               });
            
$('#resetmotif').click(function(e){
    $('#formmotif').trigger("reset");
  });

            });

$(document).on('click', '#buttoneditmotif', function(){
  $("#editsimpanmotif").prop('disabled', true);
  $("#editkodemotif").prop('disabled', true);
  $("#editnamamotif").prop('disabled', true);
  $("#editsimpanmotif").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
      $.ajax({
              url: "/master/motif/data/"+$(this).data("value"),
              method: 'get',
              success: function(result){
                $("#editsimpanmotif").html('<i class="fa fa-check-square-o"></i>Simpan');
                $("#editsimpanmotif").prop('disabled', false);
                $("#editkodemotif").prop('disabled', false);
                $("#editnamamotif").prop('disabled', false);
                $('#editidmotif').val(result.id_motif);
                $('#editkodemotif').val(result.kode_motif);
                $('#editnamamotif').val(result.nama_motif);
              }
            });
});

$(document).on('click', '#editsimpanmotif', function(){
    $('#editsimpanmotif').prop('disabled', true);
    $("#editsimpanmotif").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
    $.ajax({
              url: "{!! route('editmotifdata') !!}",
              method: 'post',
              data: {
                 kodemotif: $('#editkodemotif').val(),
                 namamotif: $('#editnamamotif').val(),
                 idmotif: $('#editidmotif').val()
              },
              success: function(result){
                $('#editsimpanmotif').prop('disabled', false);
                $("#editsimpanmotif").html('<i class="fa fa-check-square-o"></i>Simpan');
                swal('Data Tersimpan');
                if ( $.fn.dataTable.isDataTable( '#tabelmotif' ) ) {
                          table = $('#tabelmotif').DataTable();
                          table.destroy();
                      }
                      $(function() {
                          $('#tabelmotif').DataTable({
                                "processing": true,
                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                responsive: true,
                                "order": [[ 4, "desc" ]],
                                language: {
                                  "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                  "sSearchPlaceholder": "Search any..",
                                  "search": "_INPUT_",
                                },
                                serverSide: true,
                                ajax: '/master/motif/data',
                                columns: [
                                  {"data": {id_motif:"id_motif",dihapus:"dihapus"},"searchable":"false",
                                     "render": function(data, type, row, meta){
                                      if (data.dihapus === null) {
                                          var button = '<button data-toggle="modal" data-target="#formeditmotif" id="buttoneditmotif" data-value="'+data.id_motif+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeletemotif" data-value="'+data.id_motif+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                                      }else{
                                          var button = '';
                                      }
                                      return button;
                                     }
                                  },
                                  {data: 'kode_motif'},
                                  {data: 'nama_motif'},
                                  {data: 'name'},
                                  {data: 'dibuat'},
                                  {data: 'diubah'},
                                  {"data": "dihapus",
                                     "render": function(data, type, row, meta){
                                      if(data === null){
                                        data = 'Belum Dihapus';
                                      }
                                      return data;
                                     }
                                  }
                                ]
                              });
                      });
              }
            });

});

$(document).on('click', '#buttondeletemotif', function(){
  $(this).prop('disabled', true);
  $(this).html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
  var targt = $(this);
  $.ajax({
          url: "/master/motif/data/"+$(this).data("value"),
          method: 'get',
          success: function(result){
            targt.prop('disabled', false);
            targt.html('<i class="fa fa-trash"></i>');
            swal({
                  title: "Yakin Hapus?",
                  text: "Kode motif: "+result.kode_motif+" ("+result.nama_motif+").",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    $.ajax({
                            url: "/master/motif/data/hapus/"+result.id_motif,
                            method: 'get',
                            success: function(result){
                              swal("Data Telah Dihapus",{
                                      icon: "success",
                                    });
                              if ( $.fn.dataTable.isDataTable( '#tabelmotif' ) ) {
                          table = $('#tabelmotif').DataTable();
                          table.destroy();
                      }
                      $(function() {
                          $('#tabelmotif').DataTable({
                                "processing": true,
                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                responsive: true,
                                "order": [[ 4, "desc" ]],
                                language: {
                                  "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                  "sSearchPlaceholder": "Search any..",
                                  "search": "_INPUT_",
                                },
                                serverSide: true,
                                ajax: '/master/motif/data',
                                columns: [
                                  {"data": {id_motif:"id_motif",dihapus:"dihapus"},"searchable":"false",
                                     "render": function(data, type, row, meta){
                                        if (data.dihapus === null) {
                                          var button = '<button data-toggle="modal" data-target="#formeditmotif" id="buttoneditmotif" data-value="'+data.id_motif+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeletemotif" data-value="'+data.id_motif+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                                      }else{
                                          var button = '';
                                      }
                                      return button;
                                     }
                                  },
                                  {data: 'kode_motif'},
                                  {data: 'nama_motif'},
                                  {data: 'name'},
                                  {data: 'dibuat'},
                                  {data: 'diubah'},
                                  {"data": "dihapus",
                                     "render": function(data, type, row, meta){
                                      if(data === null){
                                        data = 'Belum Dihapus';
                                      }
                                      return data;
                                     }
                                  }
                                ]
                              });
                      });
                            }
                          });
                    
                  } else {
            targt.prop('disabled', false);
            targt.html('<i class="fa fa-trash"></i>');
                    swal("Hapus Data Batal");
                  }
                });
          }
        });
  
  
});
</script>
@endsection