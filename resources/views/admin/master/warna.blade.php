@extends('admin.layout.master')

@section('title')
@parent
 | warna
@stop

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <meta name="_token" content="{{csrf_token()}}" />
@endsection

@section('judul')
<section class="content-header">
      <h1>
        Master
        <small>warna</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Data</a></li>
      </ol>
    </section>
@endsection

@section('konten')
<div class="box">
            <div class="box-header">
            	<div class="pull-left">
              		<h3 class="box-title"><i class="fa fa-bookmark-o"></i> warna</h3>
              	</div>
            	<div class="pull-right">
            		<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#formtambahwarna"><i class="fa fa-pencil-square-o"></i> Tambah</a>
              	</div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabelwarna" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Aksi</th>
                  <th>Kode warna</th>
                  <th>Nama warna</th>
                  <th>Dibuat Oleh</th>
                  <th>Tanggal Dibuat</th>
                  <th>Tanggal Diubah</th>
                  <th>Tanggal Dihapus</th>
                </tr>
                </thead>
                <tbody>
                
            	<tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
<!-- ===========================      MODAL BOOTSTRAP    ======================================================================================== -->
<div id="formtambahwarna" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-check-square-o"></i> Data Warna</h4>
      </div>
      <div class="modal-body">
      	<div class="alert alert-success" id="alert" style="display:none"></div>
<form class="form-horizontal" id="formwarna">
	  <div class="form-group">
	    <label class="control-label col-sm-3">Kode warna</label>
	    <div class="col-sm-8">
	      <input type="text" class="form-control" id="kodewarna" placeholder="Kode warna">
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="control-label col-sm-3">Nama warna</label>
	    <div class="col-sm-8">
	      <input type="text" class="form-control" id="namawarna" placeholder="Nama warna">
	    </div>
	  </div>
      </div>
      <div class="modal-footer">
        <button id="simpanwarna" type="submit" name="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i>Simpan</button>
        <button id="resetwarna" type="button" class="btn btn-warning"><i class="fa fa-retweet"></i>Reset</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-window-close"></i>Tutup</button>
      </div>
    </div>
</form>
  </div>
</div>
<!-- ===========================      MODAL BOOTSTRAP    ======================================================================================== -->
<div id="formeditwarna" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-check-square-o"></i> Data Barang <small>edit</small></h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-success" id="alert" style="display:none"></div>
<form class="form-horizontal" id="formeditwarna">
    <div class="form-group">
      <label class="control-label col-sm-3">Kode warna</label>
      <div class="col-sm-8">
        <input type="hidden" class="form-control" id="editidwarna">
        <input type="text" class="form-control" id="editkodewarna">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3">Nama warna</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="editnamawarna">
      </div>
    </div>
      </div>
      <div class="modal-footer">
        <button id="editsimpanwarna" type="submit" name="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i>Simpan</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-window-close"></i>Tutup</button>
      </div>
    </div>
</form>
  </div>
</div>
@endsection

@section('script')
<!-- DataTables -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLTE-2.4.5/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('AdminLTE-2.4.5/dist/js/demo.js')}}"></script>
<!-- notify.min -->
<script src="{{asset('js/notify.min.js')}}"></script>
<script src="{{asset('js/notify.js')}}"></script>
<script src="{{asset('js/sweetalert.min.js')}}"></script>
<!-- page script -->
<script>
$('#tabelwarna').DataTable({
          "processing": true,
          "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
          responsive: true,
          "order": [[ 4, "desc" ]],
          language: {
            "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
            "sSearchPlaceholder": "Search any..",
            "search": "_INPUT_",
          },
          serverSide: true,
          ajax: '/master/warna/data',
          columns: [
            {"data": {id_warna:"id_warna",dihapus:"dihapus"},"searchable":"false",
               "render": function(data, type, row, meta){
                if (data.dihapus === null) {
                   var button = '<button data-toggle="modal" data-target="#formeditwarna" id="buttoneditwarna" data-value="'+data.id_warna+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeletewarna" data-value="'+data.id_warna+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                }else{
               	var button = '';
                }
                return button;
               }
            },
            {data: 'kode_warna'},
            {data: 'nama_warna'},
            {data: 'name'},
            {data: 'dibuat'},
            {data: 'diubah'},
            {"data": "dihapus",
               "render": function(data, type, row, meta){
                if(data === null){
                  data = 'Belum Dihapus';
                }
                return data;
               }
            }
          ]
        });

$(document).ready(function(){
            $('#simpanwarna').click(function(e){
              $('#simpanwarna').prop('disabled', true);
              $("#simpanwarna").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');              
               e.preventDefault();
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
               if ( isNaN($('#kodewarna').val()) && isNaN($('#namawarna').val()) ) {
               $.ajax({
                  url: "{!! route('tambahwarnadata') !!}",
                  method: 'post',
                  data: {
                     kodewarna: $('#kodewarna').val(),
                     namawarna: $('#namawarna').val()
                  },
                  success: function(result){
                    swal("Data Berhasil Disimpan!");
                    $('#simpanwarna').prop('disabled', false);
                    $("#simpanwarna").html('<i class="fa fa-check-square-o"></i>Simpan');
                    if ( $.fn.dataTable.isDataTable( '#tabelwarna' ) ) {
                          table = $('#tabelwarna').DataTable();
                          table.destroy();
                      }
                      $(function() {
                          $('#tabelwarna').DataTable({
                                "processing": true,
                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                responsive: true,
                                "order": [[ 4, "desc" ]],
                                language: {
                                  "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                  "sSearchPlaceholder": "Search any..",
                                  "search": "_INPUT_",
                                },
                                serverSide: true,
                                ajax: '/master/warna/data',
                                columns: [
                                  {"data": {id_warna:"id_warna",dihapus:"dihapus"},"searchable":"false",
                                     "render": function(data, type, row, meta){
                                      if (data.dihapus === null) {
                                          var button = '<button data-toggle="modal" data-target="#formeditwarna" id="buttoneditwarna" data-value="'+data.id_warna+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeletewarna" data-value="'+data.id_warna+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                                      }else{
                                          var button = '';
                                      }
                                       return button;
                                     }
                                  },
                                  {data: 'kode_warna'},
                                  {data: 'nama_warna'},
                                  {data: 'name'},
                                  {data: 'dibuat'},
                                  {data: 'diubah'},
                                  {"data": "dihapus",
                                     "render": function(data, type, row, meta){
                                      if(data === null){
                                        data = 'Belum Dihapus';
                                      }
                                      return data;
                                     }
                                  }
                                ]
                              });
                      });
                  }
              	    });
                  }else{
                        swal({
                              title: "Kesalahan!",
                              text: "Form Tidak Boleh Kosong!",
                              icon: "warning",
                              button: "Input Ulang!",
                            });
                        $("#simpanwarna").html('<i class="fa fa-check-square-o"></i>Simpan');
                        $('#kodewarna').focus(); $('#kodewarna').focus();
                        $('#simpanwarna').prop('disabled', false);    
                       }
               });
            
$('#resetwarna').click(function(e){
    $('#formwarna').trigger("reset");
  });

            });

$(document).on('click', '#buttoneditwarna', function(){
  $("#editsimpanwarna").prop('disabled', true);
  $("#editkodewarna").prop('disabled', true);
  $("#editnamawarna").prop('disabled', true);
  $("#editsimpanwarna").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
      $.ajax({
              url: "/master/warna/data/"+$(this).data("value"),
              method: 'get',
              success: function(result){
                $("#editsimpanwarna").html('<i class="fa fa-check-square-o"></i>Simpan');
                $("#editsimpanwarna").prop('disabled', false);
                $("#editkodewarna").prop('disabled', false);
                $("#editnamawarna").prop('disabled', false);
                $('#editidwarna').val(result.id_warna);
                $('#editkodewarna').val(result.kode_warna);
                $('#editnamawarna').val(result.nama_warna);
              }
            });
});

$(document).on('click', '#editsimpanwarna', function(){
    $('#editsimpanwarna').prop('disabled', true);
    $("#editsimpanwarna").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
    $.ajax({
              url: "{!! route('editwarnadata') !!}",
              method: 'post',
              data: {
                 kodewarna: $('#editkodewarna').val(),
                 namawarna: $('#editnamawarna').val(),
                 idwarna: $('#editidwarna').val()
              },
              success: function(result){
                $('#editsimpanwarna').prop('disabled', false);
                $("#editsimpanwarna").html('<i class="fa fa-check-square-o"></i>Simpan');
                swal('Data Tersimpan');
                if ( $.fn.dataTable.isDataTable( '#tabelwarna' ) ) {
                          table = $('#tabelwarna').DataTable();
                          table.destroy();
                      }
                      $(function() {
                          $('#tabelwarna').DataTable({
                                "processing": true,
                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                responsive: true,
                                "order": [[ 4, "desc" ]],
                                language: {
                                  "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                  "sSearchPlaceholder": "Search any..",
                                  "search": "_INPUT_",
                                },
                                serverSide: true,
                                ajax: '/master/warna/data',
                                columns: [
                                  {"data": {id_warna:"id_warna",dihapus:"dihapus"},"searchable":"false",
                                     "render": function(data, type, row, meta){
                                      if (data.dihapus === null) {
                                          var button = '<button data-toggle="modal" data-target="#formeditwarna" id="buttoneditwarna" data-value="'+data.id_warna+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeletewarna" data-value="'+data.id_warna+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                                      }else{
                                          var button = '';
                                      }
                                      return button;
                                     }
                                  },
                                  {data: 'kode_warna'},
                                  {data: 'nama_warna'},
                                  {data: 'name'},
                                  {data: 'dibuat'},
                                  {data: 'diubah'},
                                  {"data": "dihapus",
                                     "render": function(data, type, row, meta){
                                      if(data === null){
                                        data = 'Belum Dihapus';
                                      }
                                      return data;
                                     }
                                  }
                                ]
                              });
                      });
              }
            });

});

$(document).on('click', '#buttondeletewarna', function(){
  $(this).prop('disabled', true);
  $(this).html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
  var targt = $(this);
  $.ajax({
          url: "/master/warna/data/"+$(this).data("value"),
          method: 'get',
          success: function(result){
            targt.prop('disabled', false);
            targt.html('<i class="fa fa-trash"></i>');
            swal({
                  title: "Yakin Hapus?",
                  text: "Kode warna: "+result.kode_warna+" ("+result.nama_warna+").",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    $.ajax({
                            url: "/master/warna/data/hapus/"+result.id_warna,
                            method: 'get',
                            success: function(result){
                              swal("Data Telah Dihapus",{
                                      icon: "success",
                                    });
                              if ( $.fn.dataTable.isDataTable( '#tabelwarna' ) ) {
                          table = $('#tabelwarna').DataTable();
                          table.destroy();
                      }
                      $(function() {
                          $('#tabelwarna').DataTable({
                                "processing": true,
                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                responsive: true,
                                "order": [[ 4, "desc" ]],
                                language: {
                                  "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                  "sSearchPlaceholder": "Search any..",
                                  "search": "_INPUT_",
                                },
                                serverSide: true,
                                ajax: '/master/warna/data',
                                columns: [
                                  {"data": {id_warna:"id_warna",dihapus:"dihapus"},"searchable":"false",
                                     "render": function(data, type, row, meta){
                                        if (data.dihapus === null) {
                                          var button = '<button data-toggle="modal" data-target="#formeditwarna" id="buttoneditwarna" data-value="'+data.id_warna+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeletewarna" data-value="'+data.id_warna+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                                      }else{
                                          var button = '';
                                      }
                                      return button;
                                     }
                                  },
                                  {data: 'kode_warna'},
                                  {data: 'nama_warna'},
                                  {data: 'name'},
                                  {data: 'dibuat'},
                                  {data: 'diubah'},
                                  {"data": "dihapus",
                                     "render": function(data, type, row, meta){
                                      if(data === null){
                                        data = 'Belum Dihapus';
                                      }
                                      return data;
                                     }
                                  }
                                ]
                              });
                      });
                            }
                          });
                    
                  } else {
            targt.prop('disabled', false);
            targt.html('<i class="fa fa-trash"></i>');
                    swal("Hapus Data Batal");
                  }
                });
          }
        });
  
  
});
</script>
@endsection