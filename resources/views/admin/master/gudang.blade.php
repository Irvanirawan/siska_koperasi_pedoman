@extends('admin.layout.master')

@section('title')
@parent
 | Gudang
@stop

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <meta name="_token" content="{{csrf_token()}}" />
@endsection

@section('judul')
<section class="content-header">
      <h1>
        Master
        <small>Gudang</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Data</a></li>
      </ol>
    </section>
@endsection

@section('konten')
<div class="box">
            <div class="box-header">
            	<div class="pull-left">
              		<h3 class="box-title"><i class="fa fa-bookmark-o"></i> Gudang</h3>
              	</div>
            	<div class="pull-right">
            		<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#formtambahgudang"><i class="fa fa-pencil-square-o"></i> Tambah</a>
              	</div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="tabelgudang" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Aksi</th>
                  <th>Kode gudang</th>
                  <th>Nama gudang</th>
                  <th>Dibuat Oleh</th>
                  <th>Tanggal Dibuat</th>
                  <th>Tanggal Diubah</th>
                  <th>Tanggal Dihapus</th>
                </tr>
                </thead>
                <tbody>
                
            	<tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
<!-- ===========================      MODAL BOOTSTRAP    ======================================================================================== -->
<div id="formtambahgudang" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-check-square-o"></i> Data Gudang</h4>
      </div>
      <div class="modal-body">
      	<div class="alert alert-success" id="alert" style="display:none"></div>
<form class="form-horizontal" id="formgudang">
	  <div class="form-group">
	    <label class="control-label col-sm-3">Kode gudang</label>
	    <div class="col-sm-8">
	      <input type="text" class="form-control" id="kodegudang" placeholder="Kode gudang">
	    </div>
	  </div>
	  <div class="form-group">
	    <label class="control-label col-sm-3">Nama gudang</label>
	    <div class="col-sm-8">
	      <input type="text" class="form-control" id="namagudang" placeholder="Nama gudang">
	    </div>
	  </div>
      </div>
      <div class="modal-footer">
        <button id="simpangudang" type="submit" name="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i>Simpan</button>
        <button id="resetgudang" type="button" class="btn btn-warning"><i class="fa fa-retweet"></i>Reset</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-window-close"></i>Tutup</button>
      </div>
    </div>
</form>
  </div>
</div>
<!-- ===========================      MODAL BOOTSTRAP    ======================================================================================== -->
<div id="formeditgudang" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-check-square-o"></i> Data Barang <small>edit</small></h4>
      </div>
      <div class="modal-body">
        <div class="alert alert-success" id="alert" style="display:none"></div>
<form class="form-horizontal" id="formeditgudang">
    <div class="form-group">
      <label class="control-label col-sm-3">Kode gudang</label>
      <div class="col-sm-8">
        <input type="hidden" class="form-control" id="editidgudang">
        <input type="text" class="form-control" id="editkodegudang">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3">Nama gudang</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" id="editnamagudang">
      </div>
    </div>
      </div>
      <div class="modal-footer">
        <button id="editsimpangudang" type="submit" name="submit" class="btn btn-primary"><i class="fa fa-check-square-o"></i>Simpan</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-window-close"></i>Tutup</button>
      </div>
    </div>
</form>
  </div>
</div>
@endsection

@section('script')
<!-- DataTables -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLTE-2.4.5/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('AdminLTE-2.4.5/dist/js/demo.js')}}"></script>
<!-- notify.min -->
<script src="{{asset('js/notify.min.js')}}"></script>
<script src="{{asset('js/notify.js')}}"></script>
<script src="{{asset('js/sweetalert.min.js')}}"></script>
<!-- page script -->
<script>
$('#tabelgudang').DataTable({
          "processing": true,
          "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
          responsive: true,
          "order": [[ 4, "desc" ]],
          language: {
            "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
            "sSearchPlaceholder": "Search any..",
            "search": "_INPUT_",
          },
          serverSide: true,
          ajax: '/master/gudang/data',
          columns: [
            {"data": {id_gudang:"id_gudang",dihapus:"dihapus"},"searchable":"false",
               "render": function(data, type, row, meta){
                if (data.dihapus === null) {
                   var button = '<button data-toggle="modal" data-target="#formeditgudang" id="buttoneditgudang" data-value="'+data.id_gudang+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeletegudang" data-value="'+data.id_gudang+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                }else{
               	var button = '';
                }
                return button;
               }
            },
            {data: 'kode_gudang'},
            {data: 'nama_gudang'},
            {data: 'name'},
            {data: 'dibuat'},
            {data: 'diubah'},
            {"data": "dihapus",
               "render": function(data, type, row, meta){
                if(data === null){
                  data = 'Belum Dihapus';
                }
                return data;
               }
            }
          ]
        });

$(document).ready(function(){
            $('#simpangudang').click(function(e){
              $('#simpangudang').prop('disabled', true);
              $("#simpangudang").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');              
               e.preventDefault();
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
               if ( isNaN($('#kodegudang').val()) && isNaN($('#namagudang').val()) ) {
               $.ajax({
                  url: "{!! route('tambahgudangdata') !!}",
                  method: 'post',
                  data: {
                     kodegudang: $('#kodegudang').val(),
                     namagudang: $('#namagudang').val()
                  },
                  success: function(result){
                    swal("Data Berhasil Disimpan!");
                    $('#simpangudang').prop('disabled', false);
                    $("#simpangudang").html('<i class="fa fa-check-square-o"></i>Simpan');
                    if ( $.fn.dataTable.isDataTable( '#tabelgudang' ) ) {
                          table = $('#tabelgudang').DataTable();
                          table.destroy();
                      }
                      $(function() {
                          $('#tabelgudang').DataTable({
                                "processing": true,
                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                responsive: true,
                                "order": [[ 4, "desc" ]],
                                language: {
                                  "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                  "sSearchPlaceholder": "Search any..",
                                  "search": "_INPUT_",
                                },
                                serverSide: true,
                                ajax: '/master/gudang/data',
                                columns: [
                                  {"data": {id_gudang:"id_gudang",dihapus:"dihapus"},"searchable":"false",
                                     "render": function(data, type, row, meta){
                                      if (data.dihapus === null) {
                                          var button = '<button data-toggle="modal" data-target="#formeditgudang" id="buttoneditgudang" data-value="'+data.id_gudang+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeletegudang" data-value="'+data.id_gudang+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                                      }else{
                                          var button = '';
                                      }
                                       return button;
                                     }
                                  },
                                  {data: 'kode_gudang'},
                                  {data: 'nama_gudang'},
                                  {data: 'name'},
                                  {data: 'dibuat'},
                                  {data: 'diubah'},
                                  {"data": "dihapus",
                                     "render": function(data, type, row, meta){
                                      if(data === null){
                                        data = 'Belum Dihapus';
                                      }
                                      return data;
                                     }
                                  }
                                ]
                              });
                      });
                  }
              	    });
                  }else{
                        swal({
                              title: "Kesalahan!",
                              text: "Form Tidak Boleh Kosong!",
                              icon: "warning",
                              button: "Input Ulang!",
                            });
                        $("#simpangudang").html('<i class="fa fa-check-square-o"></i>Simpan');
                        $('#kodegudang').focus(); $('#kodegudang').focus();
                        $('#simpangudang').prop('disabled', false);    
                       }
               });
            
$('#resetgudang').click(function(e){
    $('#formgudang').trigger("reset");
  });

            });

$(document).on('click', '#buttoneditgudang', function(){
  $("#editsimpangudang").prop('disabled', true);
  $("#editkodegudang").prop('disabled', true);
  $("#editnamagudang").prop('disabled', true);
  $("#editsimpangudang").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
      $.ajax({
              url: "/master/gudang/data/"+$(this).data("value"),
              method: 'get',
              success: function(result){
                $("#editsimpangudang").html('<i class="fa fa-check-square-o"></i>Simpan');
                $("#editsimpangudang").prop('disabled', false);
                $("#editkodegudang").prop('disabled', false);
                $("#editnamagudang").prop('disabled', false);
                $('#editidgudang').val(result.id_gudang);
                $('#editkodegudang').val(result.kode_gudang);
                $('#editnamagudang').val(result.nama_gudang);
              }
            });
});

$(document).on('click', '#editsimpangudang', function(){
    $('#editsimpangudang').prop('disabled', true);
    $("#editsimpangudang").html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
    $.ajax({
              url: "{!! route('editgudangdata') !!}",
              method: 'post',
              data: {
                 kodegudang: $('#editkodegudang').val(),
                 namagudang: $('#editnamagudang').val(),
                 idgudang: $('#editidgudang').val()
              },
              success: function(result){
                $('#editsimpangudang').prop('disabled', false);
                $("#editsimpangudang").html('<i class="fa fa-check-square-o"></i>Simpan');
                swal('Data Tersimpan');
                if ( $.fn.dataTable.isDataTable( '#tabelgudang' ) ) {
                          table = $('#tabelgudang').DataTable();
                          table.destroy();
                      }
                      $(function() {
                          $('#tabelgudang').DataTable({
                                "processing": true,
                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                responsive: true,
                                "order": [[ 4, "desc" ]],
                                language: {
                                  "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                  "sSearchPlaceholder": "Search any..",
                                  "search": "_INPUT_",
                                },
                                serverSide: true,
                                ajax: '/master/gudang/data',
                                columns: [
                                  {"data": {id_gudang:"id_gudang",dihapus:"dihapus"},"searchable":"false",
                                     "render": function(data, type, row, meta){
                                      if (data.dihapus === null) {
                                          var button = '<button data-toggle="modal" data-target="#formeditgudang" id="buttoneditgudang" data-value="'+data.id_gudang+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeletegudang" data-value="'+data.id_gudang+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                                      }else{
                                          var button = '';
                                      }
                                      return button;
                                     }
                                  },
                                  {data: 'kode_gudang'},
                                  {data: 'nama_gudang'},
                                  {data: 'name'},
                                  {data: 'dibuat'},
                                  {data: 'diubah'},
                                  {"data": "dihapus",
                                     "render": function(data, type, row, meta){
                                      if(data === null){
                                        data = 'Belum Dihapus';
                                      }
                                      return data;
                                     }
                                  }
                                ]
                              });
                      });
              }
            });

});

$(document).on('click', '#buttondeletegudang', function(){
  $(this).prop('disabled', true);
  $(this).html('<i class="fa fa-spinner fa-spin fa-fw"></i>');
  var targt = $(this);
  $.ajax({
          url: "/master/gudang/data/"+$(this).data("value"),
          method: 'get',
          success: function(result){
            swal({
                  title: "Yakin Hapus?",
                  text: "Kode gudang: "+result.kode_gudang+" ("+result.nama_gudang+").",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                  if (willDelete) {
                    $.ajax({
                            url: "/master/gudang/data/hapus/"+result.id_gudang,
                            method: 'get',
                            success: function(result){
                              swal("Data Telah Dihapus",{
                                      icon: "success",
                                    });
                              if ( $.fn.dataTable.isDataTable( '#tabelgudang' ) ) {
                          table = $('#tabelgudang').DataTable();
                          table.destroy();
                      }
                      $(function() {
                          $('#tabelgudang').DataTable({
                                "processing": true,
                                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                                responsive: true,
                                "order": [[ 4, "desc" ]],
                                language: {
                                  "processing": '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>',
                                  "sSearchPlaceholder": "Search any..",
                                  "search": "_INPUT_",
                                },
                                serverSide: true,
                                ajax: '/master/gudang/data',
                                columns: [
                                  {"data": {id_gudang:"id_gudang",dihapus:"dihapus"},"searchable":"false",
                                     "render": function(data, type, row, meta){
                                        if (data.dihapus === null) {
                                          var button = '<button data-toggle="modal" data-target="#formeditgudang" id="buttoneditgudang" data-value="'+data.id_gudang+'" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o"></i></button> <button id="buttondeletegudang" data-value="'+data.id_gudang+'" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>'; 
                                      }else{
                                          var button = '';
                                      }
                                      return button;
                                     }
                                  },
                                  {data: 'kode_gudang'},
                                  {data: 'nama_gudang'},
                                  {data: 'name'},
                                  {data: 'dibuat'},
                                  {data: 'diubah'},
                                  {"data": "dihapus",
                                     "render": function(data, type, row, meta){
                                      if(data === null){
                                        data = 'Belum Dihapus';
                                      }
                                      return data;
                                     }
                                  }
                                ]
                              });
                      });
                            }
                          });
                    
                  } else {
            targt.prop('disabled', false);
            targt.html('<i class="fa fa-trash"></i>');
                    swal("Hapus Data Batal");
                  }
                });
          }
        });
  
  
});
</script>
@endsection