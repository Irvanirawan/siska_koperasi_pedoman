@extends('report.layout.master')

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}" />
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <style type="text/css">

  </style>
@endsection

@section('judul')
<section class="content-header">
      <h1>
        Daftar
        <small>Anggota</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Daftar Anggota</li>
      </ol>
    </section>
@endsection

@section('konten')
<div class="box">
            <div class="box-header">
              <h3 class="box-title">Daftar Anggota</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="statuslist" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Kode Anggota</th>
                  <th>Nama Anggota</th>
                  <th>Tanggal Daftar</th>
                  <th>Instansi</th>
                  <th>Status</th>
                  <th>Opsi</th>
                </tr>
                </thead>
                <tbody>
@foreach(DB::table("jm_pelanggan_h")->get() as $k=>$d)
                  <tr>
                      <td>{{$d->KODE_PELANGGAN}}</td>
                      <td>{{$d->NAMA_PELANGGAN}}</td>
                      <td>{{date('d-m-Y',strtotime($d->TGL_DAFTAR))}}</td>
                      <td>{{$d->KODE_INSTANSI}}</td>
                      <td align="center">
                        @if($d->STATUS_AKTIF == 'Y') <span class='label label-info'>A k t i f</span>
                        @else <span class='label label-warning'>T i d a k   A k t i f</span>
                        @endif
                      </td>
                      <td><button data-id="{{$d->KODE_PELANGGAN}}" type="button" class="btn-gpa btn btn-xs btn-warning" data-toggle="modal" data-target="#myModal"><span class="fa fa-write"></span> Ganti Password</button></td>
                  </tr> 
@endforeach
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <form action="{{route('gantipasswordanggota')}}" method="POST">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Form Ganti Password Anggota <b><span class="kodeanggota"></span></b></h4>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <label for="email">Masukan Password Baru:</label>
            <input type="hidden" name="kds" class="form-control kds" id="kds">
            <input type="text" name="pwd" class="form-control" id="pwd">
          </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
        </form>
    </div>

  </div>
</div>
@endsection

@section('script')
<!-- DataTables -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('AdminLTE-2.4.5/dist/js/demo.js')}}"></script>
<!-- loading overlay Gaspare Sganga -->
<script src="{{asset('js/loadingoverlay.min.js')}}"></script>
<!-- loading overlay Gaspare Sganga -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/webshim/1.16.0/dev/polyfiller.js"></script> -->
<!-- DataTables -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
$(".btn-gpa").on('click', function() {
  var kds = $(this).data("id");
  $('.kds').val(kds);
  $('.kodeanggota').html(kds);
});

$("#carijenis").click(function(){
  if ($('#jenis').val() != '') {
    $.LoadingOverlay("show");
  }    
});

$('#statuslist').DataTable()
</script>
@endsection