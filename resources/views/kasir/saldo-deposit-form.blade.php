@extends('report.layout.master')

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}" />
  <!-- Select2 -->
  <link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/select2/dist/css/select2.min.css')}}">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">


  <style type="text/css">
.select2-selection {
  border-radius: 0px !important;
  height: 34px !important;
  padding-bottom: 30px;
}
.dt-buttons{
  margin-left: 30px;
}
  </style>
}
@endsection

@section('judul')
<section class="content-header">
      <h1>
        Saldo 
        <small>Anggota</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Saldo Anggota</li>
      </ol>
    </section>
@endsection

@section('konten')
<!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Form Input Voucher</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

          <div class="row">
          <form class="form-horizontal" action="{{route('kasirinsert')}}" method="POST">
              {{csrf_field()}}
            <div class="col-md-12">

                <div class="form-group">
                  <label class="col-sm-2 control-label">Anggota</label>
                  <div class="col-sm-8">
                    <select class="form-control select2" style="width: 100%;" name="anggota">
                      <option selected="selected">Pilih Anggota</option>
                @foreach(DB::table('jm_pelanggan_h')->get() as $k=>$d)
                      <option value="{{$d->KODE_PELANGGAN}}">{{$d->KODE_PELANGGAN}} {{$d->NAMA_PELANGGAN}}</option>
                @endforeach
                  </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Saldo deposit</label>
                  <div class="col-sm-8">                    
                    <input type="text" class="form-control saldodeposit" placeholder="" value="" readonly disabled>
                  </div>
                </div>
              <!--   <div class="form-group">
                  <label class="col-sm-2 control-label">Nomor Voucher </label>
                  <div class="col-sm-8">
                    <input type="text" name="novoucher" class="form-control currency" required="required"/>
                  </div>
                </div> -->
                <div class="form-group">
                  <label class="col-sm-2 control-label">Deposit Yang Dipakai</label>
                  <div class="col-sm-8">
                    <input type="number" name="inputdeposit" id="inputdeposit" class="form-control currency" required="required"/>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"></label>
                  <div class="col-sm-8">
                    <button class="btn btn-info" type="submit">Simpan</button>
                  </div>
                </div>

              </div>
              <!-- /.box-body -->
              
              <!-- /.box-footer -->
          </form>
            </div>


            </div>
            <!-- /.col -->
          </div>

<!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Riwayat Input Voucher</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <div class="box-body">

          <div class="box-tools pull-right">
            <form class="form-inline" action="">
              <div class="form-group">
                <label for="email">Dari:</label>
                <input class="date_range_filter date form-control datepicker_from" value="{{date('m/d/Y')}}" type="text" id="datepicker_from" />
              </div>
              <div class="form-group">
                <label for="pwd">Sampai:</label>
                <input class="date_range_filter date form-control datepicker_to" value="{{date('m/d/Y')}}" type="text" id="datepicker_to" />
              </div>
              <button type="button" class="voucherdaterange btn btn-info btn-sm">Submit</button>
            </form>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">

          <div class="row">
          <form class="form-horizontal" action="{{route('kasirinsert')}}" method="POST">
              {{csrf_field()}}
            <div class="col-md-12">
            
                      <table id="statuslist" class=" tablevoucher table table-bordered table-striped">
                          <thead>
                          <tr>
                            <th>Tanggal Di Input</th>
                            <th>Anggota</th>
                            <th>Instansi</th>
                            <th>Nomor Voucher</th>
                            <th>Nominal</th>
                          </tr>
                          </thead>
                          <tbody>
          @foreach($data as $k=>$d)
                            <tr>
                                <td>{{$d->tgl_bukti}}</td>
                                <td>{{$d->NAMA_PELANGGAN}}</td>
                                <td>{{$d->KODE_INSTANSI}}</td>
                                <td>{{$d->no_bukti}}</td>
                                <td>{{number_format(abs($d->nominal))}}</td>
                            </tr> 
          @endforeach
                          </tbody>
                          <tfoot>
                            <tr>
                              <th colspan="3"></th>
                              <th style="text-align: center;">Total</th>
                              <th>{{number_format(abs($data->sum('nominal')))}}</th>
                            </tr>
                          </tfoot>
                        </table>

              </div>
              <!-- /.box-body -->
              
              <!-- /.box-footer -->
          </form>
            </div>


            </div>
            <!-- /.col -->
          </div>
@endsection

@section('script')
<!-- DataTables -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('AdminLTE-2.4.5/dist/js/demo.js')}}"></script>
<!-- bootstrap datepicker -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- loading overlay Gaspare Sganga -->
<script src="{{asset('js/loadingoverlay.min.js')}}"></script>
<!-- Select2 -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
<!-- loading overlay Gaspare Sganga -->
<script src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<!-- loading overlay Gaspare Sganga -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<!-- loading overlay Gaspare Sganga -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<!-- loading overlay Gaspare Sganga -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<!-- loading overlay Gaspare Sganga -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<!-- loading overlay Gaspare Sganga -->
<script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>





<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/webshim/1.16.0/dev/polyfiller.js"></script> -->
<script>
$('#datepicker_from').datepicker({
  dateFormat: 'dd-mm-yy'
});
$('#datepicker_to').datepicker({
  dateFormat: 'dd-mm-yy'
});
$('#statuslist').DataTable({
  "pageLength": 50,
  dom : 'lBfrtip',
  buttons: [
      'excelHtml5',
      'pdfHtml5'
  ]
});
$("#carijenis").click(function(){
  if ($('#jenis').val() != '') {
    $.LoadingOverlay("show");
  }    
});

$(document).ready(function(){
  $('.select2').select2();
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $(".select2").on('change', function() {
              var val = this.value;
                $.ajax({
                    /* the route pointing to the post function */
                    url: '{{url('/')}}/api/getsaldo/'+val,
                    type: 'GET',
                    beforeSend: function() {                      
                          $('.btn').prop('disabled', true);
                    },
                    /* remind that 'data' is the response of the AjaxController */
                    success: function (data) {
                          $(".saldodeposit").val(data);
                          $('.btn').prop('disabled', false);
                          document.getElementById("inputdeposit").max = data;
                    }
                }); 
            });

       });
$('.voucherdaterange').click(function(){
    var dari = $('.datepicker_from').val();
    var sampai = $('.datepicker_to').val();
if (isNaN(Date.parse(dari)) || isNaN(Date.parse(sampai))) {
  if (isNaN(Date.parse(dari))) {$('.datepicker_from').focus();}
  if (isNaN(Date.parse(sampai))) {$('.datepicker_to').focus();}  
}else{
  $.ajax
    ({ 
        url: '{!! route("loadvoucher") !!}',
        data: {"dari": dari,"sampai":sampai},
        type: 'post',
        beforeSend: function(){
          $.LoadingOverlay("show");
        },
        success: function(result)
        {
          $.LoadingOverlay("hide");
            if ($.fn.DataTable.isDataTable(".tablevoucher")) {
                $('.tablevoucher').DataTable().clear().destroy();
              }
            $('.tablevoucher').html(result);
              $(".tablevoucher").dataTable({
                "pageLength": 50,
                  dom : 'lBfrtip',
                  buttons: [
                      'excelHtml5',
                      'pdfHtml5'
                  ]
              });
        }
    });
}
    
});
</script>
@endsection

