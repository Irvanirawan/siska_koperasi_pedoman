						  <thead>
                          <tr>
                            <th>Tanggal Di Input</th>
                            <th>Anggota</th>
                            <th>Instansi</th>
                            <th>Nomor Voucher</th>
                            <th>Nominal</th>
                          </tr>
                          </thead>
                          <tbody>
          @foreach($data as $k=>$d)
                            <tr>
                                <td>{{$d->tgl_bukti}}</td>
                                <td>{{$d->NAMA_PELANGGAN}}</td>
                                <td>{{$d->KODE_INSTANSI}}</td>
                                <td>{{$d->no_bukti}}</td>
                                <td>{{number_format(abs($d->nominal))}}</td>
                            </tr> 
          @endforeach
                          </tbody>
                          <tfoot>
                            <tr>
                              <th colspan="3"></th>
                              <th style="text-align: center;">Total</th>
                              <th>{{number_format(abs($data->sum('nominal')))}}</th>
                            </tr>
                          </tfoot>