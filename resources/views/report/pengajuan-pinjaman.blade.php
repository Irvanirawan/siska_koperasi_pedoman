@extends('report.layout.master')

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}" />
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <style type="text/css">

  </style>
@endsection

@section('judul')
<section class="content-header">
      <h1>
        Pengajuan 
        <small>Pinjaman Anggota</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Pengajuan Pinjaman Anggota</li>
      </ol>
    </section>
@endsection

@section('konten')
<!-- SELECT2 EXAMPLE -->
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Pengajuan Pinjaman</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <form class="form-horizontal" acion="{{route('pengajuan-pinjaman-anggota-post')}}" method="POST">
              {{csrf_field()}}
            <div class="col-md-12">
              
              <div class="">

                <div class="form-group">
                  <label class="col-sm-2 control-label">No Bukti</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="nobukti" placeholder="{{$nobukti}}" value="{{$nobukti}}" readonly>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Tanggal Pengajuan</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="tanggalpengajuan" placeholder="{{date('d-m-Y H-i-s')}}" value="{{date('d-m-Y')}}" readonly>  
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Jenis Pinjaman</label>
                  <div class="col-sm-8">
                    <select class="form-control pinjaman" name="jenispinjaman" required="required">
                    <option>--Pilih Jenis Pinjaman--</option>
                  @foreach(DB::table('kpr_m_jenispinjaman')->get() as $jenis)
                    <option value="{{$jenis->kode_jenispinjaman}}">{{$jenis->nama_JenisPinjaman}}</option>
                  @endforeach
                  </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Besar Pinjaman</label>
                  <div class="col-sm-8">
                    <input type="number" name="besarpinjaman" class="form-control currency" id="c1" required="required"/>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Jangka Waktu (PerBulan)</label>
                  <div class="col-sm-8">
                    <select class="form-control jasapersen" name="jangkawaktu" required="required">
                    <option value="">--Pilih Jangka Waktu Pinjaman--</option>
                    <option value="">Pilih Jenis Pinjaman Dahulu</option>
                  </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label">Keperluan</label>
                  <div class="col-sm-8">
                    <textarea class="form-control" name="keperluan" rows="5" id="comment"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label"></label>
                  <div class="col-sm-8">
                    <button class="btn btn-info" type="submit">Simpan</button>
                  </div>
                </div>

              </div>
              <!-- /.box-body -->
              
              <!-- /.box-footer -->
           
            </div>

 </form>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->    
        </div>
        <!-- /.box-body -->
        
      </div>
      <!-- /.box -->
@endsection

@section('script')
<!-- DataTables -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('AdminLTE-2.4.5/dist/js/demo.js')}}"></script>
<!-- loading overlay Gaspare Sganga -->
<script src="{{asset('js/loadingoverlay.min.js')}}"></script>
<!-- loading overlay Gaspare Sganga -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/webshim/1.16.0/dev/polyfiller.js"></script> -->
<script>

$("#carijenis").click(function(){
  if ($('#jenis').val() != '') {
    $.LoadingOverlay("show");
  }    
});

$(document).ready(function(){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $(".pinjaman").on('change', function() {
              var val = this.value;
                $.ajax({
                    /* the route pointing to the post function */
                    url: '{{route('gettempo')}}',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: {_token: CSRF_TOKEN, id:val},
                    dataType: 'JSON',
                    /* remind that 'data' is the response of the AjaxController */
                    success: function (data) {
                          var $el = $(".jasapersen");
                          // $el.empty(); // remove old options
                          $('.jasapersen option:gt(0)').remove();
                          $.each(data, function(key,value) {
                            $el.append($("<option></option>")
                               .attr("value", value).text(key));
                          });
                    }
                }); 
            });
            $(".jasapersen").on('change', function() {
              var val = this.value;
              var val2 = $(".pinjaman").val();
                $.ajax({
                    /* the route pointing to the post function */
                    url: '{{route('getjasa')}}',
                    type: 'POST',
                    /* send the csrf-token and the input to the controller */
                    data: {_token: CSRF_TOKEN, kj:val2, tp:val},
                    dataType: 'JSON',
                    /* remind that 'data' is the response of the AjaxController */
                    success: function (data) {
                      var currency = $(".currency").val();
                      var jasapersen = $(".jasapersen").val();
                      var jasa = $(".jasa").val();
                          $(".jasa").val(data);
                          $(".angsuranpokok").val(currency/jasapersen);
                          $(".angsuranjasa").val((jasapersen/100)*currency);
                          $(".jumlahangsuran").val(currency/jasapersen);
                          $(".biayaadministrasi").val();
                          $(".jumlahditerima").val(currency);
                    }
                }); 
            });
            $("#c1").on('keyup', function() {
                      var currency = $(".currency").val();
                      var jasapersen = $(".jasapersen").val();
                      var jasa = $(".jasa").val();
                          $(".angsuranpokok").val(currency/jasapersen);
                          $(".angsuranjasa").val((jasapersen/100)*currency);
                          $(".jumlahangsuran").val(currency/jasapersen);
                          $(".biayaadministrasi").val();
                          $(".jumlahditerima").val(currency);
            });
       });
</script>
@endsection