<ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENU</li>
        <!-- Optionally, you can add icons to the links -->
        <li class="{{ request()->is('home') ? 'active' : '' }}"><a href="{{route('home')}}"><i class="fa fa-home"></i> <span>Beranda</span></a></li>
        @if(Session::get('user')['level'] == 'Anggota')
        <li class="{{ request()->is('report/simpanan-anggota') ? 'active' : '' }}"><a href="{{route('biodata-anggota')}}"><i class="fa fa-user"></i> <span>Biodata Anggota</span></a></li>
        <li class="{{ request()->is('report/pinjaman-anggota/') ? 'active' : '' }}"><a href="{{route('pengajuan-pinjaman-anggota')}}"><i class="fa fa-plus"></i> <span>Pengajuan Pinjaman</span></a></li>
        <li class="{{ request()->is('report/pinjaman-anggota/') ? 'active' : '' }}"><a href="{{route('status-pengajuan-pinjaman-anggota')}}"><i class="fa fa-book"></i> <span>Status Pengajuan Pinjaman</span></a></li>
        <li class="{{ request()->is('report/pinjaman-anggota/') ? 'active' : '' }}"><a href="{{route('pinjaman-anggota')}}"><i class="fa fa-money"></i> <span>Laporan Pinjaman</span></a></li>
        <li class="{{ request()->is('report/simpanan-anggota') ? 'active' : '' }}"><a href="{{route('belanja-anggota')}}"><i class="fa fa-shopping-basket"></i> <span>Laporan Belanja</span></a></li>
        <li class="{{ request()->is('report/simpanan-anggota/') ? 'active' : '' }}"><a href="{{route('simpanan-anggota')}}"><i class="fa fa-briefcase"></i> <span>Laporan Simpanan</span></a></li>
        @elseif(Session::get('user')['level'] == 'Administrator')
        <li class="{{ request()->is('report/pinjaman-anggota/') ? 'active' : '' }}"><a href="{{route('daftar-anggota')}}"><i class="fa fa-users"></i> <span>Daftar Anggota</span></a></li>
        <li class="{{ request()->is('report/pinjaman-anggota/') ? 'active' : '' }}"><a href="{{route('permintaan-pinjaman')}}"><i class="fa fa-book"></i> <span>Status Pengajuan</span></a></li>
        @elseif(Session::get('user')['level'] == 'Kasir')
        <li class="{{ request()->is('report/pinjaman-anggota/') ? 'active' : '' }}"><a href="{{route('kasir')}}"><i class="fa fa-money"></i> <span>Saldo Deposit</span></a></li>
        @else
        @endif
        <li class="{{ request()->is('report/pinjaman-anggota/') ? 'active' : '' }}"><a href="#" onclick="event.preventDefault();" data-toggle="modal" data-target="#gantipassword"><i class="fa fa-lock"></i> <span>Ganti Password</span></a></li>
        <li>
            <a href="{{url('/')}}/logout-anggota">
                <i class="fa fa-sign-out"></i><span>Log Out </span>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                {{ csrf_field() }}
            </form>
        </li>
      </ul>