<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> Koperasi </title>
  <link rel="shortcut icon" href="{{asset('media/asset/favicon.png')}}">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/dist/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/dist/css/skins/skin-blue.min.css')}}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  @yield('css')
  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini" style="font-size:16px">SISKA</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="{{asset('media/asset/LOGO-SISKA-SOFTWARE.png')}}" style="width: 200px;height: 45px;"></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
         
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="{{route('biodata-anggota')}}">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <i class="fa fa-user"> {{DB::table('jm_pelanggan_h')->where('KODE_PELANGGAN','=',Session::get('user')['kds'])->first()->NAMA_PELANGGAN}}</i>
              <span class="hidden-xs"></span>
            </a>            
          </li>
@if(Session::get('user')['level'] == 'Anggota')
          <!-- User Account Menu -->
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="{{route('biodata-anggota')}}">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <i class="">Sisa Deposit Rp. {{DB::table('jr_kartudeposit')->where('kode_pelanggan','=',Session::get('user')['kds'])->sum('nominal')}}</i>
              <span class="hidden-xs"></span>
            </a>            
          </li>
@endif
          <li class="dropdown user user-menu">
            <!-- Menu Toggle Button -->
            <a href="{{url('/')}}/logout-anggota" title="Logout">
              <!-- hidden-xs hides the username on small devices so only the image appears. -->
              <i class="fa fa-sign-out"></i>
              <span class="hidden-xs">Logout</span>
            </a>            
          </li>

        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- search form (Optional) -->
      <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
              <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
              </button>
            </span>
        </div>
      </form> -->
      <!-- /.search form -->

      <!-- Sidebar Menu -->
      @include('report.layout.menu')
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    @yield('judul')

    <!-- Main content -->
    <section class="content container-fluid">

      <!--------------------------
        | Your Page Content Here |
        -------------------------->
@yield('konten')

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      www.siskasoftware.com
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; {{date('Y')}} <a href="https://siskasoftware.com/bantuan-support/">Siska Software</a>.</strong>.
  </footer>

  <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- Modal -->
<div id="gantipassword" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Form Ganti Password</h4>
      </div>
      <div class="modal-body">
        
<form class="form-horizontal" action="{{route('gantipassword')}}" method="POST">
  <div class="form-group">
    <label class="control-label col-sm-4">Password Lama:</label>
    <div class="col-sm-8"> 
      <input type="password" name="pwd" class="form-control pwd" placeholder="Masukan password" required="required" maxlength="10">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-4">Password Baru:</label>
    <div class="col-sm-8"> 
      <input type="password" name="pwd1" class="form-control pwd1" placeholder="Masukan password baru (max 10)" required="required" maxlength="10">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-4">Konfirmasi Password Baru:</label>
    <div class="col-sm-8"> 
      <input type="password" name="pwd2" class="form-control pwd2" placeholder="Masukan password baru (max 10)" required="required" maxlength="10">
      <p class="btn-warning pesanpwd" style="display:none">Konfirmasi Password Tidak Sama</p>
    </div>
  </div>
  <div class="form-group"> 
    <div class="col-sm-offset-4 col-sm-8">
      <button type="submit" class="btn btn-default btnpwd">Submit</button>
    </div>
  </div>
</form>

      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div> -->

  </div>
</div>
<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('AdminLTE-2.4.5/dist/js/adminlte.min.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
     @yield('script')
<script type="text/javascript">
@if(Session::has('notif'))
swal("info!", "{{session::get('notif')}}", "info");
@endif
$('.pwd2').keyup(function(e){
        var pwd1 = $('.pwd1').val();
        var pwd2 = this.value;
        
        if(pwd1 == pwd2){
          $('.pesanpwd').hide();
          $('.btnpwd').prop('disabled', false);
        }else{
          $('.pesanpwd').show();
          $('.btnpwd').prop('disabled', true);
        }
      });
</script>
</body>
</html>