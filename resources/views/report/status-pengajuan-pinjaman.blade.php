@extends('report.layout.master')

@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}" />
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <style type="text/css">

  </style>
@endsection

@section('judul')
<section class="content-header">
      <h1>
        Status
        <small>Pengajuan Pinjaman Anggota</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{route('pengajuan-pinjaman-anggota')}}">Pengajuan Pinjaman Anggota</a></li>
        <li class="active">Status Pengajuan Pinjaman Anggota</li>
      </ol>
    </section>
@endsection

@section('konten')
<div class="box">
            <div class="box-header">
              <h3 class="box-title">History Pengajuan Pinjaman</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="statuslist" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No Bukti</th>
                  <th>Tanggal Pengajuan</th>
                  <th>Jenis Pinjaman</th>
                  <th>Besar Pinjaman</th>
                  <th>Jangka Waktu</th>
                  <th>Status</th>
                </tr>
                </thead>
                <tbody>
@foreach($data as $key=>$d)
                <tr>
                  <td>{{$d->NoBukti}}</td>
                  <td>{{date('d-m-Y',strtotime($d->TglBukti))}}</td>
                  <td>{{$d->nama_JenisPinjaman}}</td>
                  <td>Rp. {{number_format($d->BesarPinjaman)}}</td>
                  <td>{{$d->JangkaWaktu}}</td>
                  <td>
                        @if($d->FLAG == 'T') <span class='label label-info'>Pengajuan</span>
                        @elseif($d->FLAG == 'N') <span class='label label-warning'>Tolak</span>
                        @elseif($d->FLAG == 'Y') <span class='label label-success'>Setujui</span>
                        @else
                        @endif 
                  </td>
                </tr>
@endforeach
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
@endsection

@section('script')
<!-- DataTables -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('AdminLTE-2.4.5/dist/js/demo.js')}}"></script>
<!-- loading overlay Gaspare Sganga -->
<script src="{{asset('js/loadingoverlay.min.js')}}"></script>
<!-- loading overlay Gaspare Sganga -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/webshim/1.16.0/dev/polyfiller.js"></script> -->
<!-- DataTables -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script>
$("#carijenis").click(function(){
  if ($('#jenis').val() != '') {
    $.LoadingOverlay("show");
  }    
});

$('#statuslist').DataTable()
</script>
@endsection