@extends('report.layout.master')

@section('css')
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">

<style type="text/css">
  .page {
    display: none;
  }
  .page-active {
    display: block;
  }
</style>
@endsection()

@section('judul')
<section class="content-header">
      <h1>
        Beranda
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class="active"></li>
      </ol>
    </section>
@endsection

@section('konten')



@if(Session::get('user')['level'] == 'Administrator')
<div class="box box-default">
      <div class="box-header with-border">
        <h3 class="box-title">Form Input Info</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          <!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button> -->
        </div>
      </div>
      <div class="box-body">
      <form class="form-horizontal" method="POST" action="{{route('inputinfo')}}" enctype="multipart/form-data">
        <div class="form-group">
          <label class="control-label col-sm-2">Judul:</label>
          <div class="col-sm-10">
            <input type="text" name="judul" class="form-control" id="judul" placeholder="Masukan Judul" required="required" value="{{( Session::has('tidakpdf')) ? Session::get('tidakpdf.judul') : ''}}">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2">Keterangan:</label>
          <div class="col-sm-10">
           <textarea name="keterangan" class="form-control" rows="3" placeholder="Masukan Ketarangan ..." required="required" >{{(Session::has('tidakpdf'))?Session::get('tidakpdf.keterangan'):''}}</textarea>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-2">File:</label>
          <div class="col-sm-10">
            <input type="file" name="file" class="form-control-file" id="file" required="required">
            <!-- <p class="help-block">Example block-level help text here.</p> -->
          </div>
        </div>
        <div class="form-group"> 
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-info">Submit</button>
          </div>
        </div>
      </form>
      </div>
</div>
@endif

<div class="row">
<div class="col-md-12">
  <div class="box">
            <!-- <div class="box-header">
              <h3 class="box-title">Info Pedoman</h3>
            </div> -->
            <!-- /.box-header -->
            <div class="box-body">
              <table id="info" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th><h3>Daftar Informasi KP-RI Pedoman Kabupaten Pandeglang</h3></th>
                </tr>
                </thead>
                <tbody>
@foreach(DB::table('am_info')->get() as $k=>$d)
                <tr>
                  <td>
                  <h4 title="{{$d->judul}}"><a href="{{asset('dokumen')}}/{{$d->dokumen}}">{{$d->keterangan}}</a><span class="pull-right">
                  @if(Session::get('user')['level'] == 'Administrator')<button class="btn btn-danger buttonx" data-id="{{$d->id}}"><i class="fa fa-trash"></i></span></button>@endif</h4>
                  </td>
                </tr>
@endforeach
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
  </div>
</div>
@endsection

@section('script')

<script src="{{asset('js/loadingoverlay.min.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- DataTables -->
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('AdminLTE-2.4.5/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- loading overlay Gaspare Sganga -->
<script src="{{asset('js/loadingoverlay.min.js')}}"></script>
<script type="text/javascript">
@if (Session::has('tidakpdf'))
swal("Gagal","{{ Session::get('tidakpdf.pesan') }}","error");
@endif
@if (Session::has('pdf'))
swal("Sukses","{{ Session::get('pdf') }}","success");
@endif
$(function () {
    $('#info').DataTable({
      "bLengthChange": false,
      "bInfo": false,
      "bFilter": false,
      "bSort": false,
      "pageLength": 15
    })
  })
@if(Session::get('user')['level'] == 'Administrator')
$(document).on('click', '.buttonx', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    swal({
          title: "Konfirmasi Hapus !",
          text: "Klik OK untuk Hapus",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {          
          if (willDelete) {
            closeModal: false,
            $.ajax({
                type: "POST",
                url: "{{route('deleteinfo')}}",
                data: {id:id},
                beforeSend: function() {
                  $.LoadingOverlay("show");
                },
                success: function (data) {
                  $.LoadingOverlay("hide");
                    swal("Data Telah Dihapus!", {
                          icon: "success",
                        });
                    location.reload();
                    }         
            });      
          } else {
            swal("Hapus Batal.");
          }
        });
});
@endif
</script>
@endsection