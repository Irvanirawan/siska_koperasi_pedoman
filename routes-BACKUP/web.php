<?php

/*
|--------------------------------------------------------------------------
| Routes Non Login
|--------------------------------------------------------------------------
*/
Route::get('/', function () {
    return redirect('login-anggota');
})->name('indek');

// Route::get('resetpass/{email}/{pwd}','UsersController@resetpass')->name('resetpwd');

Route::get('/login-anggota', ['as' => 'tologin', 'uses' => 'report\reportController@tologin']);
Route::post('/login-anggota', ['as' => 'dologin', 'uses' => 'report\reportController@login']);

Route::get('/logout-anggota',function(){
  Session::forget('user');
  return redirect('/login-anggota');
});

Route::post('/ganti-password', ['as' => 'gantipassword', 'uses' => 'report\reportController@gantipassword']);

Route::get('/home', 'report\reportController@home')->name('home');

/*
|--------------------------------------------------------------------------
| Routes Ajax
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'api'], function() {
              Route::post('/gettempo', [
                          'as'    => 'gettempo',
                        'uses'  => 'Api\reportController@gettempo'
                      ]);
              Route::post('/getjasa', [
                          'as'    => 'gettempo',
                        'uses'  => 'Api\reportController@getjasa'
                      ]);
});
/*
|--------------------------------------------------------------------------
| Routes Login
|--------------------------------------------------------------------------
*/

Auth::routes();


Route::group(['prefix' => 'anggota'], function() {
// belanja anggota
              Route::get('/belanja-anggota', [
                          'as'    => 'belanja-anggota',
                        'uses'  => 'report\reportController@belanjaanggota'
                      ]);
              Route::post('/belanja-anggota', [
                          'as'    => 'belanja-anggota-post',
                        'uses'  => 'report\reportController@belanjaanggotapost'
                      ]);
              Route::get('/belanja-anggota/{dari}/{sampai}', [
                          'as'    => 'belanja-anggota-range',
                        'uses'  => 'report\reportController@belanjaanggotarange'
                      ]);
              Route::get('/belanja-anggota/{dari}/{sampai}/pdf', [
                          'as'    => 'belanja-anggota-range-pdf',
                        'uses'  => 'report\reportController@belanjaanggotarangepdf'
                      ]);
              Route::get('/belanja-anggota/{dari}/{sampai}/pdf-download', [
                          'as'    => 'belanja-anggota-range-pdf-download',
                        'uses'  => 'report\reportController@belanjaanggotarangepdfdownload'
                      ]);
// simpanan
              Route::get('/simpanan-anggota', [
                          'as'    => 'simpanan-anggota',
                          'uses'  => 'report\reportController@simpanananggota'
                      ]);
              Route::post('/simpanan-anggota', [
                          'as'    => 'simpanan-anggota-post',
                        'uses'  => 'report\reportController@simpanananggotapost'
                      ]);
              Route::get('/simpanan-anggota/{jenis}', [
                          'as'    => 'simpanan-anggota-jenis',
                        'uses'  => 'report\reportController@simpanananggotajenis'
                      ]);
              Route::get('/simpanan-anggota/{jenis}/pdf', [
                          'as'    => 'simpanan-anggota-jenis',
                        'uses'  => 'report\reportController@simpanananggotajenispdf'
                      ]);
              Route::get('/simpanan-anggota/{jenis}/pdf-download', [
                          'as'    => 'simpanan-anggota-jenis-pdf-download',
                        'uses'  => 'report\reportController@simpanananggotajenispdfdownload'
                      ]);
// pinjaman
              Route::get('/pinjaman-anggota', [
                          'as'    => 'pinjaman-anggota',
                          'uses'  => 'report\reportController@pinjamananggota'
                      ]);
              Route::post('/pinjaman-anggota', [
                          'as'    => 'pinjaman-anggota-post',
                        'uses'  => 'report\reportController@pinjamananggotapost'
                      ]);
              Route::get('/pinjaman-anggota/{jenis}', [
                          'as'    => 'pinjaman-anggota-jenis',
                        'uses'  => 'report\reportController@pinjamananggotajenis'
                      ]);
              Route::get('/pinjaman-anggota/{jenis}/pdf', [
                          'as'    => 'pinjaman-anggota-jenis',
                        'uses'  => 'report\reportController@pinjamananggotajenispdf'
                      ]);
              Route::get('/pinjaman-anggota/{jenis}/pdf-download', [
                          'as'    => 'pinjaman-anggota-jenis-pdf-download',
                        'uses'  => 'report\reportController@pinjamananggotajenispdfdownload'
                      ]);
// pengajuan pinjaman
              Route::get('/pengajuan-pinjaman-anggota', [
                          'as'    => 'pengajuan-pinjaman-anggota',
                          'uses'  => 'pinjaman\pengajuanpinjamanController@index'
                      ]);
              Route::get('/status-pengajuan-pinjaman-anggota', [
                          'as'    => 'status-pengajuan-pinjaman-anggota',
                          'uses'  => 'pinjaman\pengajuanpinjamanController@indexstatus'
                      ]);
              Route::post('pengajuan-pinjaman-anggota', [
                          'as'    => 'pengajuan-pinjaman-anggota-post',
                        'uses'  => 'pinjaman\pengajuanpinjamanController@store'
                      ]);
              Route::get('/pengajuan-pinjaman-anggota/{jenis}', [
                          'as'    => 'pengajuan-pinjaman-anggota-jenis',
                        'uses'  => 'pinjaman\pengajuanpinjamanController@pinjamananggotajenis'
                      ]);
              Route::get('/pengajuan-pinjaman-anggota/{jenis}/pdf', [
                          'as'    => 'pengajuan-pinjaman-anggota-jenis',
                        'uses'  => 'pinjaman\pengajuanpinjamanController@pinjamananggotajenispdf'
                      ]);
              Route::get('/pengajuan-pinjaman-anggota/{jenis}/pdf-download', [
                          'as'    => 'pengajuan-pinjaman-anggota-jenis-pdf-download',
                        'uses'  => 'pinjaman\pengajuanpinjamanController@pinjamananggotajenispdfdownload'
                      ]);
// Biodata
              Route::get('/biodata-anggota', [
                          'as'    => 'biodata-anggota',
                          'uses'  => 'report\reportController@biodataanggota'
                      ]);
});