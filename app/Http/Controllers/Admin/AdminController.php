<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App;
use PDF;
use Session;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function daftaranggota()
    {
        if (!Session::has('user')) {
            return redirect('/login-anggota');
        }
        return view('admin.daftar-anggota');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function statuspinjaman()
    {
        if (!Session::has('user')) {
            return redirect('/login-anggota');
        }
        return view('admin.status-pengajuan');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function statuspinjamanaksi(Request $request)
    {
        if ($request->aksi == 1) {
           $aksi = 'Y';
        }else{
            $aksi = 'N';
        }
        DB::table('kpr_t_pengajuanpinjaman')->where('NoBukti','=',$request->id)->update(['FLAG'=>$aksi]);
        return redirect()->back();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function kasir()
    {
        $dari = date('Y-m-d');
        $sampai = date('Y-m-d');
        if (!Session::has('user')) {
            return redirect('/login-anggota');
        }
        $data = DB::table("jr_kartudeposit")
                ->leftjoin("jm_pelanggan_h",'jr_kartudeposit.kode_pelanggan','=','jm_pelanggan_h.KODE_PELANGGAN')
                ->select('jr_kartudeposit.*','jm_pelanggan_h.NAMA_PELANGGAN','jm_pelanggan_h.KODE_INSTANSI')
                ->where('jr_kartudeposit.nominal','<',0)                
                ->whereBetween('jr_kartudeposit.tgl_bukti', [$dari, $sampai])
                ->get();
        return view('kasir.saldo-deposit-form',['data'=>$data]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getsaldo($kode)
    {
        if (!Session::has('user')) {
            return redirect('/login-anggota');
        }
        $saldo = DB::table('jr_kartudeposit')->where('kode_pelanggan','=',$kode)->sum('nominal');
        echo $saldo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function kasirinsert(Request $request)
    {
        $saldo_anggota = DB::table('jr_kartudeposit')->where('kode_pelanggan','=',$request->anggota)->sum('nominal');
        $nomorvoucher = DB::table('jr_kartudeposit')->orderBy('no_bukti','DESC')->first()->no_bukti + 1;
        if ($request->inputdeposit > $saldo_anggota) { // saldo tidak cukup
            Session::flash('notif','Saldo Tidak Cukup');
        }else{
                // DB::table('jt_depositpelanggan')->insert([
                //         'no_bukti'=>$nomorvoucher,
                //         'tgl_bukti'=>date('Y-m-d'),
                //         'kode_pelanggan'=>$request->anggota,
                //         'nominal'=>$request->inputdeposit,
                //         'cara_bayar'=>"Kas Tunai",
                //         'jenis'=>""
                //     ]);
                $nom = $num = -1 * abs($request->inputdeposit);
                    DB::table('jr_kartudeposit')->insert([
                        'tgl_bukti'=>date('Y-m-d'),
                        'no_bukti'=>$nomorvoucher,
                        'kode_pelanggan'=>$request->anggota,
                        'nominal'=>$nom
                    ]);
                Session::flash('notif','Sukses Tersimpan');
        }

        return redirect()->back();
    }

    public function loadvoucher(Request $request)
    {
        $dari = date('Y-m-d',strtotime($request->dari));
        $sampai = date('Y-m-d',strtotime($request->sampai));
        $data = DB::table('jr_kartudeposit')->whereBetween('tgl_bukti', [$dari, $sampai])->get();
        $data = DB::table("jr_kartudeposit")
                ->leftjoin("jm_pelanggan_h",'jr_kartudeposit.kode_pelanggan','=','jm_pelanggan_h.KODE_PELANGGAN')
                ->select('jr_kartudeposit.*','jm_pelanggan_h.NAMA_PELANGGAN','jm_pelanggan_h.KODE_INSTANSI')
                ->where('jr_kartudeposit.nominal','<',0)
                ->whereBetween('jr_kartudeposit.tgl_bukti', [$dari, $sampai])
                ->get();
        return view('kasir.load', ['data' => $data])->render();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function gantipasswordanggota(Request $request)
    {
        DB::table('as_password')->where('KODE_STAFF','=',$request->kds)->update(['password'=>$request->pwd]);
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function inputinfo(Request $req)
    {
        if ($req->file('file')->getClientOriginalExtension() != "pdf") {
            Session::flash('tidakpdf', ['pesan'=>'Jenis File Wajib PDF','judul'=>$req->judul,'keterangan'=>$req->keterangan]);
        }else{
            $jumlah = DB::table('am_info')->count()+1;
            $nama = $jumlah.'-'.$req->file('file')->getClientOriginalName();
            // $req->file('file')->storeAs('dokumen',$nama);
            $req->file('file')->move(base_path().'/public/dokumen/', $nama);
            DB::table('am_info')->insert(['judul'=>$req->judul,
                                            'keterangan'=>$req->keterangan,
                                            'dokumen'=>$nama
                                        ]);
            Session::flash('pdf', 'Berhasil Disimpan'); 
        }
        
        return redirect()->back();
    }
    public function deleteinfo(Request $request)
    {

        // User::find($request->id)->delete();
        DB::table('am_info')->where('id','=',$request->id)->delete();
        return redirect()->back()->with('success','Telah Dihapus');
    }
}
