<?php

namespace App\Http\Controllers\Admin\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use DataTables;
use Illuminate\Support\Facades\Auth;

class ArtikelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(Auth::User()->name);
        return view('admin.master.artikel');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function data()
    {
        $data = DB::table('im_artikel')->leftjoin('users','users.id','=','im_artikel.id_user')->select('im_artikel.*','users.name')->get();
        return Datatables::of($data)->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->kodeartikel;
        // $grocery = new Grocery();
        // $grocery->name = $request->name;
        // $grocery->type = $request->type;
        // $grocery->price = $request->price;

        // $grocery->save();
        DB::table('im_artikel')->insert([
                                'kode_artikel'=>$request->kodeartikel,
                                'nama_artikel'=>$request->namaartikel,
                                'id_user'=>Auth::User()->id,
                                'dibuat'=>date('Y-m-d H:i:s'),
                                'diubah'=>date('Y-m-d H:i:s')
                            ]);
        return response()->json(['success'=>'Data Berhasil Disimpan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = DB::table('im_artikel')->where('id_artikel','=',$id)->first();
        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('im_artikel')->where('id_artikel','=',$request->idartikel)->update([
                                'kode_artikel'=>$request->kodeartikel,
                                'nama_artikel'=>$request->namaartikel,
                                'id_user'=>Auth::User()->id,
                                'diubah'=>date('Y-m-d H:i:s')        
            ]);
        return response()->json(['success'=>'Data Berhasil Dirubah']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('im_artikel')->where('id_artikel','=',$id)->update([
                                'dihapus'=>date('Y-m-d H:i:s')        
            ]);
        return response()->json(['success'=>'Data Berhasil Dihapus']);
    }
}
