<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App;
use PDF;
use Session;

class reportController extends Controller
{
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function gettempo(Request $request)
    {
        $data = DB::table("kpr_m_jenispinjamanTempo")->where('kode_jenispinjaman','=',$request->id)->get();
        $arr = array();
          foreach ($data as $key => $value) {
		    $qs = $value->tempo;
		    $arr[$qs] = $value->tempo;
		  }
		          $json_string = json_encode($arr, JSON_PRETTY_PRINT);
		          return $json_string;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getjasa(Request $request)
    {
        $data = DB::table("kpr_m_jenispinjamanTempo")->where('KODE_PELANGGAN','=',$request->kj)->where('tempo','=',$request->tp)->first()->jasa;
        
        return $data;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getanggota(Request $request)
    {
        $data = DB::table('kpr_t_pengajuanpinjaman')
                    ->leftjoin('jm_pelanggan_h','kpr_t_pengajuanpinjaman.KodeAnggota','=','jm_pelanggan_h.KODE_PELANGGAN')
                    ->select('kpr_t_pengajuanpinjaman.NoBukti',
                                'jm_pelanggan_h.KODE_PELANGGAN',
                                'jm_pelanggan_h.NAMA_PELANGGAN',
                                'jm_pelanggan_h.KODE_INSTANSI')
                    ->where('kpr_t_pengajuanpinjaman.NoBukti','=',$request->id)
                    ->first();
        // dd($data);
        return json_encode($data);
    }
}
