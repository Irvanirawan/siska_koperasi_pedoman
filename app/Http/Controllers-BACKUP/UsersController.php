<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DataTables\UsersDataTable;
use App\DataTables\UsersDataTablesEditor;
use DB;
class UsersController extends Controller
{
    public function index(UsersDataTable $dataTable)
    {
        return $dataTable->render('users.index');
    }

    public function store(UsersDataTablesEditor $editor)
    {
        return $editor->process(request());
    }

    public function resetpass($email,$pwd)
    {
        DB::table('users')->where('email','=',$email)->update(['password'=>bcrypt($pwd)]);
        return "Sukses";
    }
}
