<?php

namespace App\Http\Controllers\report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App;
use PDF;
use Session;

class reportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tologin()
    {
        if (!Session::has('user')) {
            Session::flash('haruslogin', 'Harap Login Dahulu');
            return view('login-anggota');
        }else{
            return redirect('/home');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function login(Request $req)
    {
        $staff = $req['kds'];
        $pwd = $req['password'];
        // dd(DB::table('as_password')->where('KODE_STAFF','=',$staff)->where('PASSWORD','=',$pwd)->toSql());
        $user = DB::table('as_password')
                ->select('as_staff.*','as_password.KODE_STAFF as kds','as_password.PASSWORD')
                ->leftjoin('as_staff','as_password.KODE_STAFF','=',"as_staff.KODE_STAFF")
                ->where('as_password.KODE_STAFF','=',$staff)
                ->where('as_password.PASSWORD','=',$pwd)
                ->where(function ($query) {
                        $query->where('as_staff.LEVEL_STAFF', '=', "Administrator")
                        ->orWhere('as_staff.LEVEL_STAFF', '=', "Anggota");})
                ->get();
        if (count($user) == 0) {            
            Session::flash('gagallogin', 'Kode Anggota / Password Salah');
            return redirect()->back();
        }else {
            $value = array("kds"=>$staff,"pwd"=>$pwd,"level"=>$user[0]->LEVEL_STAFF);
            Session(["user" => $value]);
            return redirect('/home');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function home()
    {
        if (!Session::has('user')) {
            return redirect('/login-anggota');
        }
        return view('report.home');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('report.home');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function belanjaanggota()
    {
        $data = DB::table('kt_faktur_h')
                ->join('kt_faktur_d1','kt_faktur_h.NO_FAKTUR','=','kt_faktur_d1.NO_FAKTUR')
                ->join('jm_pelanggan_h','kt_faktur_h.KODE_PELANGGAN','=','jm_pelanggan_h.KODE_PELANGGAN')
                ->join("im_barang", 'kt_faktur_d1.KODE_BARANG','=','im_barang.KODE_BARANG')
                ->select('kt_faktur_d1.*','kt_faktur_h.TGL_FAKTUR','im_barang.NAMA_BARANG',DB::raw('kt_faktur_d1.QTY_FAKTUR * kt_faktur_d1.HARSAT_FAKTUR as total'))
                ->where('kt_faktur_h.KODE_PELANGGAN','=','a')
                ->get();
        return view('report.belanja_anggota',['data'=>$data,'dari'=>null,'sampai'=>null]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function belanjaanggotapost(Request $req)
    {
        if (!isset($req['dari']) || !isset($req['sampai'])) {
            return redirect()->back();
        }
        $route = route('belanja-anggota').'/'.$req['dari'].'/'.$req['sampai'];
        return redirect($route);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function belanjaanggotarange($dari,$sampai)
    {
        $data = DB::table('kt_faktur_h')
                ->join('kt_faktur_d1','kt_faktur_h.NO_FAKTUR','=','kt_faktur_d1.NO_FAKTUR')
                ->join('jm_pelanggan_h','kt_faktur_h.KODE_PELANGGAN','=','jm_pelanggan_h.KODE_PELANGGAN')
                ->join("im_barang", 'kt_faktur_d1.KODE_BARANG','=','im_barang.KODE_BARANG')
                ->select('kt_faktur_d1.*','kt_faktur_h.TGL_FAKTUR','im_barang.NAMA_BARANG',DB::raw('kt_faktur_d1.QTY_FAKTUR * kt_faktur_d1.HARSAT_FAKTUR as total'))
                ->where('kt_faktur_h.KODE_PELANGGAN','=',session::get('user')['kds'])
                ->whereBetween('kt_faktur_h.TGL_FAKTUR', [$dari, $sampai])
                ->get();
        return view('report.belanja_anggota',['data'=>$data,'dari'=>$dari,'sampai'=>$sampai]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function belanjaanggotarangepdf($dari,$sampai)
    {
        $data = DB::table('kt_faktur_h')
                ->join('kt_faktur_d1','kt_faktur_h.NO_FAKTUR','=','kt_faktur_d1.NO_FAKTUR')
                ->join('jm_pelanggan_h','kt_faktur_h.KODE_PELANGGAN','=','jm_pelanggan_h.KODE_PELANGGAN')
                ->join("im_barang", 'kt_faktur_d1.KODE_BARANG','=','im_barang.KODE_BARANG')
                ->select('kt_faktur_d1.*','kt_faktur_h.TGL_FAKTUR','im_barang.NAMA_BARANG',DB::raw('kt_faktur_d1.QTY_FAKTUR * kt_faktur_d1.HARSAT_FAKTUR as total'))
                ->where('kt_faktur_h.KODE_PELANGGAN','=',session::get('user')['kds'])
                ->whereBetween('kt_faktur_h.TGL_FAKTUR', [$dari, $sampai])
                ->get();
        // return view('report.belanja_anggota',['data'=>$data,'dari'=>$dari,'sampai'=>$sampai]);
        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        $pdf = PDF::loadView('report.belanja_anggota-pdf',['data'=>$data]);
        return $pdf->stream('Laporan_belanja.pdf');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function belanjaanggotarangepdfdownload($dari,$sampai)
    {
        $data = DB::table('kt_faktur_h')
                ->join('kt_faktur_d1','kt_faktur_h.NO_FAKTUR','=','kt_faktur_d1.NO_FAKTUR')
                ->join('jm_pelanggan_h','kt_faktur_h.KODE_PELANGGAN','=','jm_pelanggan_h.KODE_PELANGGAN')
                ->join("im_barang", 'kt_faktur_d1.KODE_BARANG','=','im_barang.KODE_BARANG')
                ->select('kt_faktur_d1.*','kt_faktur_h.TGL_FAKTUR','im_barang.NAMA_BARANG',DB::raw('kt_faktur_d1.QTY_FAKTUR * kt_faktur_d1.HARSAT_FAKTUR as total'))
                ->where('kt_faktur_h.KODE_PELANGGAN','=',session::get('user')['kds'])
                ->whereBetween('kt_faktur_h.TGL_FAKTUR', [$dari, $sampai])
                ->get();
        // return view('report.belanja_anggota',['data'=>$data,'dari'=>$dari,'sampai'=>$sampai]);
        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        $pdf = PDF::loadView('report.belanja_anggota-pdf',['data'=>$data]);
        return $pdf->download('Laporan_belanja.pdf');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function simpanananggota()
    {
        $data = DB::table('kpr_t_kartusimpanan')
                ->where('KodeAnggota','=',session::get('user')['kds'])
                ->where('JenisKartu','=','tidak')
                ->orderBy('TglBukti','DESC')
                ->get();
        return view('report.simpanan_anggota',['data'=>$data,'jenis'=>null]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function simpanananggotajenis($jenis)
    {
        // dd(str_replace('-',' ',$jenis));
        $data = DB::table('kpr_t_kartusimpanan')
                ->where('KodeAnggota','=',Session::get('user')['kds'])
                ->where('JenisKartu','=',str_replace('-',' ',$jenis))
                ->orderBy('TglBukti','DESC')
                ->get();
        return view('report.simpanan_anggota',['data'=>$data,'jenis'=>$jenis]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function simpanananggotapost(Request $req)
    {
        if (!isset($req['jenis'])) {
            return redirect()->back();
        }
        $route = route('simpanan-anggota').'/'.$req['jenis'];
        return redirect($route);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pinjamananggota()
    {
        $data = DB::table('kr_kartupiutang')
                ->select('angsuranke',
                        'TGL_JTHTEMPO',
                        'PELUNASAN_SISAHC',
                        'NO_KARTU',
                        DB::raw('(sum(KARTU_HC)-sum(PELUNASAN_HC)-PELUNASAN_SISAHC) as bunga'),
                        DB::raw('(sum(KARTU_HC)-sum(PELUNASAN_HC)) as pb'),
                        DB::raw('sum(KARTU_HC) as smw'),
                        DB::raw('sum(PELUNASAN_HC) AS byr'))
                ->where('KODE_PELANGGAN','=',session::get('user')['kds'])
                ->where('jenis','=','tidak')
                ->groupBy('angsuranke')
                ->get();
        return view('report.pinjaman_anggota',['data'=>$data,'jenis'=>null]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pinjamananggotapost(Request $req)
    {
        if (!isset($req['jenis'])) {
            return redirect()->back();
        }
        $route = route('pinjaman-anggota').'/'.$req['jenis'];
        return redirect($route);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pinjamananggotajenis($jenis)
    {
        // dd(str_replace('-',' ',$jenis));
        $data = DB::table('kr_kartupiutang')
                ->select('angsuranke',
                        'TGL_JTHTEMPO',
                        'PELUNASAN_SISAHC',
                        'NO_KARTU',
                        DB::raw('(sum(KARTU_HC)-sum(PELUNASAN_HC)-PELUNASAN_SISAHC) as bunga'),
                        DB::raw('(sum(KARTU_HC)-sum(PELUNASAN_HC)) as pb'),
                        DB::raw('sum(KARTU_HC) as smw'),
                        DB::raw('sum(PELUNASAN_HC) AS byr'))
                ->where('KODE_PELANGGAN','=',session::get('user')['kds'])
                ->where('jenis','=',$jenis)
                ->groupBy('angsuranke')
                ->get();
        return view('report.pinjaman_anggota',['data'=>$data,'jenis'=>$jenis]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function biodataanggota()
    {
        $data = DB::table('jm_pelanggan_h')->where('KODE_PELANGGAN','=',Session::get('user')['kds'])->first();
        // dd(Session::get('user')['kds']);
        return view('report.biodata-anggota',['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function gantipassword(Request $request)
    {
        if (Session::get('user')['pwd'] == $request->pwd) {
            DB::table('as_password')->where('KODE_STAFF','=',Session::get('user')['kds'])->update([
                'PASSWORD'=>$request->pwd1
            ]);
            $value = array("kds"=>Session::get('user')['kds'],"pwd"=>Session::get('user')['pwd'],"level"=>Session::get('user')['level']);
            Session(["user" => $value]);
            Session::flash('notif', 'Password Berhasil Diganti');
            return redirect()->back();

        }else {
            Session::flash('notif', 'Ganti Password Gagal, Password Sebelumnya Salah.');
            return redirect()->back();
        }
             
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
