<?php

namespace App\Http\Controllers\pinjaman;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App;
use PDF;
use Session;

class pengajuanpinjamanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // TPP{{((int)explode("-",DB::table('kpr_t_pengajuanpinjaman')->orderBy('NoBukti', 'DESC')->limit(1)->first()->NoBukti))+1}}
        $nobukti = 'TPP'.(count(DB::table('kpr_t_pengajuanpinjaman')->get())+1);
        return view('report.pengajuan-pinjaman',['nobukti'=>$nobukti]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexstatus()
    {
        $data = DB::table('kpr_t_pengajuanpinjaman')
                    ->leftjoin('kpr_m_jenispinjaman','kpr_m_jenispinjaman.kode_jenispinjaman','=','kpr_t_pengajuanpinjaman.kode_JenisPinjaman')
                    ->select('kpr_t_pengajuanpinjaman.*','kpr_m_jenispinjaman.nama_JenisPinjaman')
                    ->where('kpr_t_pengajuanpinjaman.KodeAnggota','=',Session::get('user')['kds'])
                    ->get();
        return view('report.status-pengajuan-pinjaman',['data'=>$data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        DB::table("kpr_t_pengajuanpinjaman")->insert([
                        "NoBukti"           =>$request->nobukti,
                        "TglBukti"          =>Date('Y-m-d H:i:s'),
                        "kode_JenisPinjaman"=>$request->jenispinjaman,
                        "KodeAnggota"       =>Session::get('user')['kds'],
                        "BesarPinjaman"     =>$request->besarpinjaman,
                        "JangkaWaktu"       =>$request->jangkawaktu,
                        "Bunga"             =>$request->jasa,
                        "AngsuranPokok"     =>$request->angsuranpokok,
                        "AngsuranBunga"     =>$request->angsuranjasa,
                        "JumlahAngsuran"    =>$request->angsuranpokok + $request->angsuranjasa,
                        "JumlahDiterima"    =>$request->jumlahditerima,
                        "BiayaAdministrasi" =>$request->jumlahditerima,
                        "operator"          =>'Pengajuan Melaui Web',
                        "keterangan"        =>$request->keperluan,
                        "FLAG"              =>"T"
            ]);
        Session::flash('notif', 'Pengajuan Telah Terkirim, klik menu -Status Pengajuan Pinjaman- untuk melihat status pengajuan anda.');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
