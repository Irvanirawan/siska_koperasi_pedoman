<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'http://localhost/*','http://192.168.88.33/pedoman/public/*','https://36.91.140.61/pedoman/*','http://36.91.140.61/pedoman/*','36.91.140.61/pedoman/*'
    ];
}
